//package com.jl.papago;
//
//import java.util.List;
//
//import javax.annotation.Resource;
//
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.test.context.ContextConfiguration;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//import org.springframework.util.Assert;
//
//import com.jl.papago.dao.PapagoDAO;
//import com.jl.papago.vo.PapagoInfoVO;
//
//import lombok.extern.slf4j.Slf4j;
//
//@RunWith(SpringJUnit4ClassRunner.class)
//@ContextConfiguration(locations= {
//	"file:src/main/webapp/WEB-INF/spring/root-context.xml"
//})
//@Slf4j
//public class PapagoInfoDAOTest {
//	// TDD 개발론 // Alt + Shift + X 그리고 T
//	
//	@Resource
//	private PapagoDAO pdao;
//	
//	@Test
//	public void test() {
//		Assert.notNull(pdao, "should not be 'null'!");
//		List<PapagoInfoVO> piList = pdao.selectPapagoInfoVOList(null);
//		Assert.isTrue(piList.size()==1, "사이즈가 1이지!");
//	}
//
//}
