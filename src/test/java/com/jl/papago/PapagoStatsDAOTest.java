//package com.jl.papago;
//
//import java.util.List;
//
//import javax.annotation.Resource;
//
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.test.context.ContextConfiguration;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//import org.springframework.util.Assert;
//
//import com.jl.papago.dao.PapagoStatsDAO;
//import com.jl.papago.vo.PapagoStatsVO;
//
//import lombok.extern.slf4j.Slf4j;
//
//@RunWith(SpringJUnit4ClassRunner.class)
//@ContextConfiguration(locations= {
//	"classpath:root-context.xml"
//})
//
//@Slf4j
//public class PapagoStatsDAOTest {
//	@Resource
//	private PapagoStatsDAO psdao;
//	
//	@Test
//	public void test() {
//		List<PapagoStatsVO> psList = psdao.selectPapagoStatsList(null);
//		Assert.isTrue(psList.size()==1, "조회 잘 됨");
//	}
//	
//	@Test
//	public void insert() {
//		PapagoStatsVO ps = new PapagoStatsVO();
//		ps.setUiNum(61);
//		ps.setPiNum(41);
//		int cnt = psdao.insertPapagoStats(ps);
//		Assert.isTrue(cnt==1, "입력 잘 됨");
//	}
//}
