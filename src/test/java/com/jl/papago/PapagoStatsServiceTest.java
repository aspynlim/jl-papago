//package com.jl.papago;
//
//import java.util.List;
//import java.util.Map;
//
//import javax.annotation.Resource;
//
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.test.context.ContextConfiguration;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//import org.springframework.util.Assert;
//
//import com.jl.papago.service.PapagoStatsService;
//import com.jl.papago.vo.PapagoStatsVO;
//
//import lombok.extern.slf4j.Slf4j;
//
//@RunWith(SpringJUnit4ClassRunner.class)
//@ContextConfiguration(locations= {
//	"classpath:root-context.xml"
//})
//
//@Slf4j
//public class PapagoStatsServiceTest {
//	
//	@Resource
//	private PapagoStatsService psService;
//	
//	@Test
//	public void test() {
//		insert();
//		List<PapagoStatsVO> psList = select();
//		Assert.isTrue(psList.size() == 2, "입력 조회 잘됨");
//	}
//	
//	public Map<String, String> insert(){
//		PapagoStatsVO ps = new PapagoStatsVO();
//		ps.setUiNum(61);
//		ps.setPiNum(42);
//		return psService.insertPapagoStats(ps);
//	}
//	public List<PapagoStatsVO> select(){
//		return psService.selectPapagoStatsList(null);
//	}
//}
