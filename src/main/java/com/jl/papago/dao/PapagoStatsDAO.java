package com.jl.papago.dao;

import java.util.List;

import com.jl.papago.vo.PapagoStatsVO;

public interface PapagoStatsDAO {
	public int insertPapagoStats(PapagoStatsVO ps);
	public List<PapagoStatsVO> selectPapagoStatsList(PapagoStatsVO ps);
	public List<PapagoStatsVO> selectPapagoStatsListByDate(PapagoStatsVO ps);
	public List<PapagoStatsVO> selectPapagoStatsListByUser(PapagoStatsVO ps);
}
