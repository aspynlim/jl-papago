package com.jl.papago.dao;

import java.util.List;

import com.jl.papago.vo.PapagoInfoVO;

public interface PapagoDAO {
	public List<PapagoInfoVO> selectPapagoInfoVOList(PapagoInfoVO tvo);
	public PapagoInfoVO selectPapagoInfo(PapagoInfoVO tvo);
	public int insertPapagoInfo(PapagoInfoVO tvo);
//	public int updatePapagoInfo(PapagoInfoVO tvo);
	public int updatePapagoInfoForCnt(PapagoInfoVO tvo);
	public int deletePapagoInfo(PapagoInfoVO tvo);
}
