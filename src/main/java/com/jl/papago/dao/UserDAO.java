package com.jl.papago.dao;

import com.jl.papago.vo.UserInfoVO;

public interface UserDAO {
	public UserInfoVO doLogin(UserInfoVO ui);
	public int insertUserInfo(UserInfoVO ui);
}
