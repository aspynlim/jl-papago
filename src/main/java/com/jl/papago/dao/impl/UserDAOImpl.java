package com.jl.papago.dao.impl;

import javax.annotation.Resource;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.stereotype.Repository;

import com.jl.papago.dao.UserDAO;
import com.jl.papago.vo.UserInfoVO;

import lombok.extern.slf4j.Slf4j;

@Repository
@Slf4j
public class UserDAOImpl implements UserDAO {
	
	@Resource
	private SqlSessionFactory ssf;
	
	@Override
	public UserInfoVO doLogin(UserInfoVO user) {
		SqlSession ss = ssf.openSession();
		try {
			return ss.selectOne("com.jl.papago.dao.UserInfoMapper.doLogin", user);
		} catch(Exception e) {
			log.error(e.getMessage());
		} finally {
			ss.close();
		}
		// TODO Auto-generated method stub
		log.info("ssf=>{}", ssf);
		return null;
	}

	@Override
	public int insertUserInfo(UserInfoVO user) {
		SqlSession ss = ssf.openSession();
		try {
			int cnt = ss.insert("com.jl.papago.dao.UserInfoMapper.insertUserInfo", user);
			ss.commit();
			return cnt;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			ss.close();
		}
		return 0;
	}

}
