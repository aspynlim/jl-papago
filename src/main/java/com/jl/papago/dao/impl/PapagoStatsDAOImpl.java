package com.jl.papago.dao.impl;

import java.util.List;

import javax.annotation.Resource;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.stereotype.Repository;

import com.jl.papago.dao.PapagoStatsDAO;
import com.jl.papago.vo.PapagoStatsVO;

import lombok.extern.slf4j.Slf4j;

@Repository
@Slf4j
public class PapagoStatsDAOImpl implements PapagoStatsDAO {

	@Resource
	private SqlSessionFactory ssf;	// 의존성 주입
	
	@Override
	public int insertPapagoStats(PapagoStatsVO ps) {
		SqlSession ss = ssf.openSession(false);
		try {
			int cnt = ss.insert("com.jl.papago.dao.PapagoStatsMapper.insertPapagoStats", ps);
			ss.commit();
			return cnt;
		}catch(Exception e) {
			ss.rollback();
			log.error(e.getMessage());
		}finally {
			ss.close();
		}
		return 0;
	}
	
	@Override
	public List<PapagoStatsVO> selectPapagoStatsList(PapagoStatsVO ps) {
		SqlSession ss = ssf.openSession();
		try {
			return ss.selectList("com.jl.papago.dao.PapagoStatsMapper.selectPapagoStatsList", ps);
		}catch(Exception e) {
			log.error(e.getMessage());
		}finally {
			ss.close();
		}
		return null;
	}
	
	@Override
	public List<PapagoStatsVO> selectPapagoStatsListByDate(PapagoStatsVO ps) {
		SqlSession ss = ssf.openSession();
		try {
			return ss.selectList("com.jl.papago.dao.PapagoStatsMapper.selectPapagoStatsListByDate", ps);
		}catch(Exception e) {
			log.error(e.getMessage());
		}finally {
			ss.close();
		}
		return null;
	}

	@Override
	public List<PapagoStatsVO> selectPapagoStatsListByUser(PapagoStatsVO ps) {
		SqlSession ss = ssf.openSession();
		try {
			return ss.selectList("com.jl.papago.dao.PapagoStatsMapper.selectPapagoStatsListByUser", ps);
		}catch(Exception e) {
			log.error(e.getMessage());
		}finally {
			ss.close();
		}
		return null;
	}
}
