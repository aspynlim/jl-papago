package com.jl.papago.dao.impl;

import java.util.List;

import javax.annotation.Resource;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.stereotype.Repository;

import com.jl.papago.dao.PapagoDAO;
import com.jl.papago.vo.PapagoInfoVO;

@Repository
public class PapagoDAOImpl implements PapagoDAO {

	// 의존성 주입
	@Resource
	private SqlSessionFactory ssf; // root-context.xml
	
	@Override
	public List<PapagoInfoVO> selectPapagoInfoVOList(PapagoInfoVO tvo) {
		SqlSession ss = ssf.openSession();
		try {
			return ss.selectList("com.jl.papago.dao.PapagoInfoMapper.selectPapagoInfoList", tvo);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			ss.close();
		}
		return null;
	}

	@Override
	public PapagoInfoVO selectPapagoInfo(PapagoInfoVO tvo) {
		SqlSession ss = ssf.openSession();
		try {
			return ss.selectOne("com.jl.papago.dao.PapagoInfoMapper.selectPapagoInfo", tvo);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			ss.close();
		}
		return null;
	}

	@Override
	public int insertPapagoInfo(PapagoInfoVO tvo) {
		SqlSession ss = ssf.openSession();
		try {
			int cnt = ss.insert("com.jl.papago.dao.PapagoInfoMapper.insertPapagoInfo", tvo);
			ss.commit();
			return cnt;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			ss.close();
		}
		return 0;
	}
	
	@Override
	public int updatePapagoInfoForCnt(PapagoInfoVO tvo) {
		SqlSession ss = ssf.openSession();
		try {
			int cnt = ss.update("com.jl.papago.dao.PapagoInfoMapper.updatePapagoInfoForCnt", tvo);
			ss.commit();
			return cnt;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			ss.close();
		}
		return 0;
	}

//	@Override
//	public int updatePapagoInfo(PapagoInfoVO tvo) {
//		SqlSession ss = ssf.openSession();
//		try {
//			int cnt = ss.update("com.jl.papago.dao.PapagoInfoMapper.updatePapagoInfo", tvo);
//			ss.commit();
//			return cnt;
//		} catch (Exception e) {
//			e.printStackTrace();
//		} finally {
//			ss.close();
//		}
//		return 0;
//	}

	@Override
	public int deletePapagoInfo(PapagoInfoVO tvo) {
		SqlSession ss = ssf.openSession();
		try {
			int cnt = ss.delete("com.jl.papago.dao.PapagoInfoMapper.deletePapagoInfo", tvo);
			ss.commit();
			if(cnt != tvo.getPiNums().length) {
				ss.rollback();
				return 0;
			}
			return cnt;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			ss.close();
		}
		return 0;
	}

	public static void main(String[] args) {
		
	}
}
