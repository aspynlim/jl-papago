package com.jl.papago.sample;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class PropertiesReader2 {
//	public static BufferedReader getBufferedReaderFromFile(String path) {
//		// 여기에 빼줘도 됨
//	}
	
	public static void main(String[] args) {
		String path = "C:\\Users\\Administrator\\eclipse-workspace\\jl-papago\\src\\main\\resources\\test.properties";
		// "\"는 "\"\"" 이렇게 벗어나게 하기 위해서 경로 구분자를 표시하기 위해 "\"를 한번 더 붙여주는 것
		// "\t"는 tab으로 인식
		
		System.out.println(path); // "client.id=1234=4" 이게 나와야 하는데....
		
		try {
			FileInputStream fis = new FileInputStream(path);
			InputStreamReader isr = new InputStreamReader(fis);
			BufferedReader br = new BufferedReader(isr);
			// 위 세줄을 getBufferedReaderFromFile(path); 여기 넣어서 불러와도 됨
			
			String str = null;
			Map<String, String> map = new HashMap<>();
			
			while((str=br.readLine())!=null) {
				int idx = str.indexOf("=");
//				map.put(str.substring(0, idx), str.substring(idx));
//				map.forEach((key, value) -> System.out.println("key : " + key + " , value : " + value));
				if(idx != -1) {
					map.put(str.substring(0, idx), str.substring(idx));
				}
			}
			System.out.println(map);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		// try-catch 안 하고 throw exception을 하면 exception 메세지가 뜸
		
		
		String str = "1,2,3,4";
		String[] strs = str.split(",");
		for(String s:strs) {
//			System.out.println(s);
		}
		
		String str2 = "client.id=1234=4";
		int idx = str2.indexOf("=");
//		System.out.println(str2.substring(0, idx));
//		System.out.println(str2.substring(idx));
	}
}
