package com.jl.papago.sample;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

// properties가 이걸 자동으로 해줘서 필요가 없다.
/*
 * client.id=1234=4
 * client.api.url=https://openapi.naver.com/v1/papago/n2mt
 */

public class PropertiesReader {
	public static void main(String[] args) {
		String path = "test.properties";
		try {
//			FileInputStream fis = new FileInputStream(path);
//			InputStreamReader isr = new InputStreamReader(fis);
//			BufferedReader br = new BufferedReader(isr);
			
			InputStream fis = PropertiesReader.class.getClassLoader().getResourceAsStream(path);
			Properties prop = new Properties();
			prop.load(fis);
			
			System.out.println(prop.getProperty("client.id")); 
			System.out.println(prop.getProperty("client.api.url")); 
			
		} catch(FileNotFoundException e) {
			e.printStackTrace();
		} catch(IOException e) {
			e.printStackTrace();
		}
	}
}

//1234=4
//https://openapi.naver.com/v1/papago/n2mt
