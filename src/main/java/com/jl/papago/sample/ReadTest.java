package com.jl.papago.sample;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

import com.jl.papago.vo.Human;

public class ReadTest {
	
	private Human h;
	private Human h2 = new Human();
	
	// main은 특별하게 동작 . 메모리 생성해 줌
	public static void main(String[] args) {
		// 폴더에 있으면 "(폴더이름)/conf.properties" 라고 하면됨
		InputStream is = ReadTest.class.getClassLoader().getResourceAsStream("conf.properties");
		InputStreamReader isr = new InputStreamReader(is);
		BufferedReader br = new BufferedReader(isr);
		
		String str = null;
		Map<String, String> ioc = new HashMap<>(); 
		
		try {
			while((str=br.readLine())!= null) {
//				if(str.split("=")[0].contentEquals("h")) {
//					ioc.put(str.split("=")[0], str.split("=")[1]);	// [0] test=1234, [1] h=vo.Human
//				}
//				ReadTest rt = new ReadTest();
//				rt.h = (Human)Test2.dependencyInjection("h", ioc));
				System.out.println("str " + str);
			}
			ReadTest rt = new ReadTest();
			System.out.println(rt.h);	// null
			System.out.println(rt.h2);	// com.jl.papago.vo.Human@4e25154f (hash code)
		} catch(IOException e) {
			e.printStackTrace();
		}
	}
}
