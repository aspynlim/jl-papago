package com.jl.papago.sample;

public class Test {
	public static void main(String[] args) {
		String className = "vo.Human"; // 클래스가 어떻게 구성되어 있는지
		try {
			Class clazzz = Class.forName(className);
			System.out.println(clazzz);
			
			Object obj = clazzz.newInstance();
			System.out.println(obj);
		} catch(ClassNotFoundException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
	}
}
