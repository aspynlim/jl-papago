package com.jl.papago.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/test")
public class TestController {
	public static String name="가을";
	
	@GetMapping("test")
	public String doTest(Model m) {
		// request에 attribute로 넘기는 대신 Model을 사용
		m.addAttribute("name", TestController.name);
		return "/test/test";
	}
	
	@PostMapping("name")
	public @ResponseBody Map<String, String> changeName(@RequestParam Map<String, String> param){
		Map<String, String> rMap = new HashMap<>();
		rMap.put("msg", "이름 변경!");
		TestController.name = param.get("name");
		return rMap;
	}
	
//	@GetMapping(value="ajax", produces="application/text;charset=utf-8")
//	public @ResponseBody String doTest() {
//		return "welcome, jane 안녕";
//	}
}
