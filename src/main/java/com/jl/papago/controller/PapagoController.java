package com.jl.papago.controller;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.jl.papago.service.PapagoService;
import com.jl.papago.vo.Message;
import com.jl.papago.vo.PapagoInfoVO;
import com.jl.papago.vo.TransVO;
import com.jl.papago.vo.UserInfoVO;

import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
public class PapagoController {
	
//	private PapagoService ps1 = new PapagoServiceImpl();
	// 메모리 생성해서 가지만, PapagoServiceImpl에서 @Resource를 사용해 private PapagoInfoDAO pidao를 사용하려고 할 때 에러남. 
	// @Resource를 못 읽어들이므로
	
	@Resource
	private PapagoService ps;
	
	@PostMapping("/papago")
	public Message doTranslate(@RequestBody TransVO tvo, HttpSession hs) {
		log.info("tvo=>{}", tvo);
		UserInfoVO ui = (UserInfoVO)hs.getAttribute("ui");	// UserServiceImpl에서 뭐라고 해줬는지
		tvo.setUiNum(ui.getUiNum());
		return ps.doTranslate(tvo);
	}
	
	@GetMapping("/papago")
	public List<PapagoInfoVO> getPapagoInfo(@ModelAttribute PapagoInfoVO param){
		log.info("param=>{}", param);
		return ps.getPapagoVOList(param);
	}
	
	@DeleteMapping("/papago")
	public Map<String, String> deleteUser(@ModelAttribute PapagoInfoVO param){
		log.info("param=>{}", param);
		return ps.deletePapagoInfo(param);
	}
	
	// 장점 & 단점 : 이름이 같지 않아도 상관 없음
	@PostMapping("/papago/param")
	public Message doTranslate(@RequestParam Map<String, String> param) {
		log.info("param=>{}", param);
		return null;
//		return ps.doTranslate(param);
	}
}
