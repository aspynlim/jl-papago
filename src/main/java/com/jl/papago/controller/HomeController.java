package com.jl.papago.controller;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.jl.papago.service.UserService;

@Controller
public class HomeController {
		
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(Locale locale, Model model) {
		
		Date date = new Date();
		DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);
		String formattedDate = dateFormat.format(date);
		
		model.addAttribute("serverTime", formattedDate );
		return "home";
	}
	
	@GetMapping("/views/**")
	public String goPage(HttpServletRequest req) {
		String uri = req.getRequestURI();
		return uri.substring(6);
	}
	
	@GetMapping("/jspTest/HelloWorld")
	public String helloWorld() {
		return "/jspTest/HelloWorld";
	}

	@GetMapping("/jspTest/test/includeTest2")
	public String includeTest2() {
		return "/jspTest/test/includeTest2";
	}	
	
	@GetMapping("/jspTest/test/includeTest")
	public String includeTest() {
		return "/jspTest/test/includeTest";
	}
	
	@GetMapping("/jspTest/tester")
	public String tester() {
		return "/jspTest/tester";
	}
	
	@GetMapping("/jspTest/tester2")
	public String tester2() {
		return "/jspTest/tester2";
	}
	
	@GetMapping("/jspTest/tester3")
	public String tester3() {
		return "/jspTest/tester3";
	}
	
	@PostMapping("/jspTest/tester5/tester4")
	public String tester4() {
		return "/jspTest/tester4";
	}	
	
	@GetMapping("/jspTest/tester5")
	public String tester5() {
		return "/jspTest/tester5";
	}
	
	@GetMapping("/jspTest/tester6")
	public String tester6() {
		return "/jspTest/tester6";
	}
	
	@GetMapping("/jspTest/tester7")
	public String tester7() {
		return "/jspTest/tester7";
	}
	
	@Resource
	private UserService us;
	
	@GetMapping("/jspTest/tester8")
	public String tester8() {
		return "/jspTest/tester8";
	}
	
	@GetMapping("/jspTest/tester9")
	public String tester9() {
		return "/jspTest/tester9";
	}
}
