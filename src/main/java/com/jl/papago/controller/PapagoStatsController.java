package com.jl.papago.controller;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jl.papago.service.PapagoStatsService;
import com.jl.papago.vo.PapagoStatsVO;

@RestController
public class PapagoStatsController {

	@Resource
	private PapagoStatsService psService;
	
	@GetMapping("/papago/stats")
	public List<PapagoStatsVO> getPapagoStatsList(PapagoStatsVO ps){
		return psService.selectPapagoStatsList(ps);
	}
	@GetMapping("/papago/stats/date")
	public List<PapagoStatsVO> selectPapagoStatsListByDate(PapagoStatsVO ps){
		return psService.selectPapagoStatsListByDate(ps);
	}
	@GetMapping("/papago/stats/user")
	public List<PapagoStatsVO> selectPapagoStatsListByUser(PapagoStatsVO ps){
		return psService.selectPapagoStatsListByUser(ps);
	}		
}
