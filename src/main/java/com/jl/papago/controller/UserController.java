package com.jl.papago.controller;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.jl.papago.service.UserService;
import com.jl.papago.vo.UserInfoVO;

import lombok.extern.slf4j.Slf4j;

// 아니면 @Controller로 한다음 
@RestController
@Slf4j
public class UserController {

	@Resource
	private UserService us;	// Service에 @Service를 붙여줘야 함
	
	@PostMapping("/login")
	public Map<String, Object> doLogin(@RequestBody UserInfoVO ui, HttpSession hs){ 
		// 안 써주면 @ModelAttribute로 인식
		// HttpSession 자동으로 session을 넣어줌 (PageContext, ... session)
		log.info("us => {}", us);
		log.info("session => {}", hs);
		log.info("user info=>{}", ui);
		// 두개 출력하고 싶으면 log.info("user info=>{}, {}", ui, ui);
		return us.doLogin(ui, hs);
	}
	
	@PostMapping("/logout")
	public Map<String, String> doLogout(HttpSession hs) {
		log.info("session => {}", hs);
		Map<String, String> rMap = new HashMap<>();
		// Invalidate the session and removes any attribute related to it
		hs.invalidate();
		rMap.put("result", "true");
		return rMap;
	}
	
	@PostMapping("/signup")
	public Map<String, String> doSignup(@RequestBody UserInfoVO ui){ 
		log.info("us => {}", us);
		log.info("user info=>{}", ui);
		return us.doSignup(ui);
	}
}

//@Controller
//public class UserController {
//
//	@Getmapping("/test")
//	public String test() {
//		// resolver를 자동으로 타서 .jsp가 붙음 (responsebody가 없어서)
//		return "test";
//	}
//	
//	// 이건 화면에 "test"가 찍힘
//	public @ResponseBody String test() {
//		return "test";
//	}
//}
