package com.jl.papago.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ChatController {
	private static List<String> strList = new ArrayList<>();
	private static String target="";
	
	@GetMapping("/init")
	public String init() {
		String target = "";
		Random r = new Random();
		while(target.length() < 3) {
			int num = r.nextInt(10);
			System.out.println("num : " + num);
			
			System.out.println("target : " + target);
			if(target.indexOf(num+"") == -1) {
				target += num;
				System.out.println("target (더한 후) : " + target);
			}
		}
		System.out.println("target : " + target);
		return "complete";
	}
	
	@PostMapping("/game")
	public Map<String, String> doGame(@RequestParam Map<String, String> param){
		String source = param.get("source");
		
		int out = 0;
		int strike = 0;
		int ball = 0;
		
		for(int i=0; i<source.length(); i++) {
//			System.out.println(i + " : " + source.charAt(i));
			int idx = target.indexOf(source.charAt(i));
			if(idx == -1) {
				out++;
			} else {
				if(i == idx) {
					strike++;
				} else {
					ball++;
				}
			}
		}
		// 원래 서비스에서 해야 함 이 작업
		Map<String, String> rMap = new HashMap<>();
		rMap.put("out", out+"");
		rMap.put("strike", strike+"");
		rMap.put("ball", ball+"");
		return rMap;
	}
	
	@PostMapping("/msg")
	public List<String> doMsg(@RequestParam Map<String, String> param){
		String id = param.get("id");
		String msg = param.get("msg") + "\n\r";
		strList.add(id + "$:v" + msg);
		return strList;
	}
	
	@GetMapping("/msg")
	public List<String> getMsg(){
		return strList;
	}
	
	public static void main(String[] args) {
		String target = "";
		Random r = new Random();
		while(target.length() < 3) {
			int num = r.nextInt(10);
			System.out.println("num : " + num);
			
			System.out.println("target : " + target);
			if(target.indexOf(num+"") == -1) {
				target += num;
				System.out.println("target (더한 후) : " + target);
			}
		}
		System.out.println("target : " + target);
	}
}
