package com.jl.papago.service;

import java.util.List;
import java.util.Map;

import com.jl.papago.vo.Message;
import com.jl.papago.vo.PapagoInfoVO;
import com.jl.papago.vo.TransVO;

public interface PapagoService {
	public Message doTranslate(TransVO tvo);
	
	public List<PapagoInfoVO> getPapagoVOList(PapagoInfoVO tvo);
	public PapagoInfoVO getPapagoVO(PapagoInfoVO tvo);
	public Map<String, String> insertPapagoInfo(PapagoInfoVO tvo);
//	public Map<String, String> updatePapagoInfo(PapagoInfoVO tvo);
	public Map<String, String> deletePapagoInfo(PapagoInfoVO tvo);
	/*
	 * 메세지 형태
	 * {
	 * 	 "message":{
	 * 	   "@type": "response",
	 * 	   ...
	 *     "result": {
	 *    	 "srLangType" : "en",
	 *       ...
	 *     }
	 *   }
	 * }
	 */
}
