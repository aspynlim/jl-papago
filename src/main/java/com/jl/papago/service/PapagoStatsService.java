package com.jl.papago.service;

import java.util.List;
import java.util.Map;

import com.jl.papago.vo.PapagoStatsVO;

public interface PapagoStatsService {
	public List<PapagoStatsVO> selectPapagoStatsList(PapagoStatsVO ps);
	public List<PapagoStatsVO> selectPapagoStatsListByDate(PapagoStatsVO ps);
	public List<PapagoStatsVO> selectPapagoStatsListByUser(PapagoStatsVO ps);
	public Map<String,String> insertPapagoStats(PapagoStatsVO ps);
}
