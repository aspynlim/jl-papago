package com.jl.papago.service;

import java.util.Map;

import javax.servlet.http.HttpSession;

import com.jl.papago.vo.UserInfoVO;

public interface UserService {
	public Map<String, Object> doLogin(UserInfoVO ui, HttpSession hs);
	public Map<String, String> doSignup(UserInfoVO ui);
}
