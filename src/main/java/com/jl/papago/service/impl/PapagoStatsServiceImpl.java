package com.jl.papago.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.jl.papago.dao.PapagoStatsDAO;
import com.jl.papago.service.PapagoStatsService;
import com.jl.papago.vo.PapagoStatsVO;

@Service
public class PapagoStatsServiceImpl implements PapagoStatsService {
	
	@Resource
	private PapagoStatsDAO psdao;
	
	@Override
	public Map<String, String> insertPapagoStats(PapagoStatsVO ps) {
		Map<String, String> rMap = new HashMap();
		rMap.put("msg", "실패!!");
		int cnt = psdao.insertPapagoStats(ps);
		if(cnt == 1) {
			rMap.put("msg", "성공!");
		}
		rMap.put("cnt", cnt+"");
		return null;
	}
	
	@Override
	public List<PapagoStatsVO> selectPapagoStatsList(PapagoStatsVO ps) {		
		return psdao.selectPapagoStatsList(ps);
	}
	
	@Override
	public List<PapagoStatsVO> selectPapagoStatsListByDate(PapagoStatsVO ps) {
		return psdao.selectPapagoStatsListByDate(ps);
	}

	@Override
	public List<PapagoStatsVO> selectPapagoStatsListByUser(PapagoStatsVO ps) {
		return psdao.selectPapagoStatsListByUser(ps);
	}
}
