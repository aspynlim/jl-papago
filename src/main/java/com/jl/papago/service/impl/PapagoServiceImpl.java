package com.jl.papago.service.impl;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jl.papago.dao.PapagoDAO;
import com.jl.papago.dao.PapagoStatsDAO;
import com.jl.papago.service.PapagoService;
import com.jl.papago.vo.Message;
import com.jl.papago.vo.PapagoInfoVO;
import com.jl.papago.vo.PapagoStatsVO;
import com.jl.papago.vo.Result;
import com.jl.papago.vo.TransVO;

import lombok.extern.slf4j.Slf4j;

@Service("papagoService")
@Slf4j
public class PapagoServiceImpl implements PapagoService {
	// src/main/resources > conf.properties에서 받아오는 것
	@Value("${client.id}")
	private String id;
	@Value("${client.secret}")
	private String secret;
	@Value("${client.api.url}")
	private String apiUrl;
	
	@Resource
	private PapagoDAO pdao;
	
	@Resource
	private PapagoStatsDAO psdao;
	
	// 양식에 안 맞는거는 무시해라
	private ObjectMapper om = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
	
	@Override
	public Message doTranslate(TransVO tvo) {
		// 처음 (TransVO ==> PapagoInfoVO) 검색한게 database에 있는지 확인
		try {
			PapagoStatsVO ps = new PapagoStatsVO();
			ps.setUiNum(tvo.getUiNum());
			log.debug("tvo=>{}",tvo);
			
			PapagoInfoVO pi = new PapagoInfoVO();
			pi.setPiSource(tvo.getSource());
			pi.setPiTarget(tvo.getTarget());
			pi.setPiText(tvo.getText());
			// 양 끝 space는 trim을 해주는게...
			pi = pdao.selectPapagoInfo(pi);
			
			if(pi != null) {
				Result r = new Result();
				r.setSrcLangType(pi.getPiSource());
				r.setTarLangType(pi.getPiTarget());
				r.setTranslatedText(pi.getPiResult());
				Message m = new Message();
				m.setResult(r);
				pdao.updatePapagoInfoForCnt(pi);
				
				// DB에 있었을 때
				ps.setPiNum(pi.getPiNum());	// 누군가가 이미 한 번역
				psdao.insertPapagoStats(ps);
				return m;
			}
			// 끝
			/*
			 * 종이컵 전화기 : 송신 - 수신 
			 */
				
			// 보내는 부분 시작
			String text = URLEncoder.encode(tvo.getText(), "UTF-8");
			URL url = new URL(apiUrl); 	// url 형태를 갖추고 있는지만 물어봄
			HttpURLConnection hc = (HttpURLConnection)url.openConnection();
			hc.setRequestMethod("POST");
			hc.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
			hc.setRequestProperty("X-Naver-Client-Id", id);
			hc.setRequestProperty("X-Naver-Client-Secret", secret);
		
			// 1. variables 2. map 3. object
			String param = "source=" + tvo.getSource() + "&target=" + tvo.getTarget() + "&text=" + text;
			// true로 해야 parameter를 보낼 수 있다. (default가 false)
			hc.setDoOutput(true);
			
			DataOutputStream dos = new DataOutputStream(hc.getOutputStream());
			dos.writeBytes(param);	// 말하는 시점
			dos.flush();
			dos.close(); // 여기서 flush가 자동으로 일어나긴 하지만
			// 보내는 부분 끝 (전화기는 연결되어 있는데 송신구만 끊은 것 / 할말 다했다.) (hc를 끊어야 완전히 끊긴 것)
		
			// 받는 부분 시작 (원래 조건을 걸어야 함)
			int status = hc.getResponseCode();	// status (code가 오는 시점이 이미 '4')
			InputStreamReader isr = new InputStreamReader(hc.getInputStream());
			BufferedReader br = new BufferedReader(isr);	// 한글자씩 안 끊고 라인마다 읽으려고
			StringBuffer res = new StringBuffer();	// 속도가 훨 빨라서! 
			
			String str = null;
			while((str=br.readLine())!=null) {	// 라인마다 읽기
				res.append(str);
			}
			br.close(); // 메모리를 다 소진했지만, 전화기를 끊어야 함 (잉여 자원을 없애기 위해)
			
			TransVO resultTvo = om.readValue(res.toString(), TransVO.class);
			log.info(res.toString());
			log.info("tvo => {}", resultTvo);
			
			// 시작 (없으면 Naver API에 갔다와야 되니까)
			Result r = resultTvo.getMessage().getResult();
			pi = new PapagoInfoVO();
			pi.setPiSource(r.getSrcLangType());
			pi.setPiTarget(r.getTarLangType());
			pi.setPiResult(r.getTranslatedText());
			pi.setPiText(tvo.getText());
			pdao.insertPapagoInfo(pi);
			log.info("piNum => {}", pi);
			
			// DB에 없었을 때
			ps.setPiNum(pi.getPiNum());	
			psdao.insertPapagoStats(ps);
			return resultTvo.getMessage();
			// 끝
	
			// 받는 부분 끝
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (MalformedURLException e) {
			e.printStackTrace();	// url이 잘못됐다면
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public List<PapagoInfoVO> getPapagoVOList(PapagoInfoVO tvo) {
		return pdao.selectPapagoInfoVOList(tvo);
	}

	@Override
	public PapagoInfoVO getPapagoVO(PapagoInfoVO tvo) {
		return pdao.selectPapagoInfo(tvo);
	}

	@Override
	public Map<String, String> insertPapagoInfo(PapagoInfoVO tvo) {
		Map<String, String> rMap = new HashMap<String, String>();
		rMap.put("cnt", pdao.insertPapagoInfo(tvo) + "");
		System.out.println(rMap);
		return rMap;
	}

//	@Override
//	public Map<String, String> updatePapagoInfo(PapagoInfoVO tvo) {
//		Map<String, String> rMap = new HashMap<String, String>();
//		rMap.put("cnt", pdao.updatePapagoInfo(tvo) + "");
//		System.out.println(rMap);
//		return rMap;
//	}

	@Override
	public Map<String, String> deletePapagoInfo(PapagoInfoVO tvo) {
		Map<String, String> rMap = new HashMap<String, String>();
		rMap.put("cnt", pdao.deletePapagoInfo(tvo) + "");
		System.out.println(rMap);
		return rMap;
	}
}
