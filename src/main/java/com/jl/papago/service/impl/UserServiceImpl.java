package com.jl.papago.service.impl;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Service;

import com.jl.papago.dao.UserDAO;
import com.jl.papago.service.UserService;
import com.jl.papago.util.SHAUtil;
import com.jl.papago.vo.UserInfoVO;

@Service
public class UserServiceImpl implements UserService {

	@Resource
	private UserDAO udao;

	@Override
	public Map<String, Object> doLogin(UserInfoVO ui, HttpSession hs) {
		// login session 넘기는거
		ui.setUiPwd(SHAUtil.getSHA(ui.getUiPwd()));
		ui = udao.doLogin(ui);
		Map<String, Object> rMap = new HashMap<>();
		if(ui == null) {
			rMap.put("msg", "Login Failed !");
			rMap.put("result", false);
		} else {
			rMap.put("msg", "Welcome, " + ui.getUiName());
			rMap.put("result", true);
			rMap.put("ui", ui);
			hs.setAttribute("ui", ui);
		}
		return rMap;
	}
	
	@Override
	public Map<String, String> doSignup(UserInfoVO ui) {
		Map<String, String> rMap = new HashMap<String, String>();
		ui.setUiPwd(SHAUtil.getSHA(ui.getUiPwd()));
		rMap.put("cnt", udao.insertUserInfo(ui) + "");
		System.out.println(rMap);
		return rMap;
	}
}
