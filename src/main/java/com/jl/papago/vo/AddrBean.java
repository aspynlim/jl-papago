package com.jl.papago.vo;

public class AddrBean {
	private String username;
	private String tel;
	private String email;
	private String gen;
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	
	public String getTel() {
		return tel;
	}
	public void setTel(String tel) {
		this.tel = tel;
	}
	
	public String getEmail() {
		return username;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getGen() {
		return gen;
	}
	public void setGen(String gen) {
		this.gen = gen;
	}
	
	@Override
	public String toString() {
		return "AddrBean [username=" + username + ", tel=" + tel + ", email=" + email + ", gen=" + gen + "]";
	}
}
