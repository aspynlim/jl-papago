package com.jl.papago.vo;

import lombok.Data;

@Data
public class UserInfoVO {
	private Integer uiNum;
	private Integer[] uiNums;
	private String uiName;
	private String active;
	private String uiId;
	private String uiPwd;
	private String moddat;
	private String modtim;
	private String credat;
	private String cretim;
	private String[] search;
	private String searchStr;
}
