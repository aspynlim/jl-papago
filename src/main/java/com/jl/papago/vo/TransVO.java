package com.jl.papago.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true) 	
public class TransVO {
	private String source;
	private String target;
	private String text;
	private Message message;
	private Integer uiNum;
}

// "@JsonIgnoreProperties"
//message에 존재하지 않는 key값 무시 : @가 딸려와서
// "fasterxml"로 import!
// 아니면, map으로 해도 됨 이건 변수명에 @가 들어가도 되니깐
// @ : config 적인 의미?!