package com.jl.papago.vo;

import lombok.Data;

@Data
public class PapagoStatsVO {
	private Integer psNum;
	private Integer uiNum;
	private Integer piNum;
	private String credat;
	private String uiId;
	private Integer cnt;
	private String order;
	
	private PageVO page = new PageVO();
}
