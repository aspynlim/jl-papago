package com.jl.papago.vo;

import lombok.Data;

@Data
public class Message {
	// 이렇게 변수명을 만들 수 없음
//	private String @type;
	private Result result;
}