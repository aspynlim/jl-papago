package com.jl.papago.vo;

import lombok.Data;

@Data
public class PapagoInfoVO {
	private Integer piNum;
	private Integer[] piNums;
	private String piSource;
	private String piTarget;
	private String piText;
	private String piResult;
	private Integer piCnt;
	private String[] search;
	private String searchStr;
	private String order;
	
	private PageVO page = new PageVO();

//	public Integer getPiCnt() {
//		return piCnt;
//	}
//	public void setPiCnt(Integer piCnt) {
//		this.piCnt = piCnt;
//	}
//	@Override
//	public String toString() {
//		return "PapagoInfoVO [piNum=" + piNum + ", piNums=" + Arrays.toString(piNums) + ", piSource=" + piSource
//				+ ", piTarget=" + piTarget + ", piText=" + piText + ", piResult=" + piResult + ", piCnt=" + piCnt
//				+ ", search=" + Arrays.toString(search) + ", searchStr=" + searchStr + "]";
//	}
}
