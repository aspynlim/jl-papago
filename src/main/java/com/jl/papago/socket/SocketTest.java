package com.jl.papago.socket;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;
import javax.websocket.EndpointConfig;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.extern.slf4j.Slf4j;

//@ServerEndpoint(value="/ws/chat", configurator=ServletConfig.class)
@Slf4j
public class SocketTest {
	
	private ObjectMapper om;
	
	// map이 더 편하긴 함
	private static List<Session> ssList = new ArrayList<>();
	
	// client에서 해당 서버에 연결을 맺었을 때
	@OnOpen
	public void open(Session ss, EndpointConfig ec) {	// HTTP session이 아니라 websocket session
		HttpSession hs = (HttpSession)ec.getUserProperties().get("hs");
	}
	
	@OnMessage
	public void message(String msg, Session ss) {
		for(Session ss1:ssList) {
			// 상대방이 보낸 거
			if(ss1 != ss) {
				try {
					ss1.getBasicRemote().sendText(msg);
				} catch(IOException e) {
					e.printStackTrace();
				}
			} else {	// 내가 보낸 거
				try {
					ss1.getBasicRemote().sendText(msg);
				} catch(IOException e) {
					e.printStackTrace();
				}				
			}
		}
	}
	
	@OnClose
	public void close(Session ss) {
		ssList.remove(ss); // f5눌러도 안 끊기고 가지고 있는 애들만 다시 보냄
		log.info(ss.getId());
	}
	
	@OnError
	public void error(Throwable err){
		log.error("err => {}", err);
	}
}
