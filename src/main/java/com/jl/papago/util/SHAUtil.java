package com.jl.papago.util;

import cc.duduhuo.util.digest.Digest;

public class SHAUtil {
	private final static String SALT_KEY="letshashpasswords";
	
	public static String getSHA(String source) {
		String target = Digest.sha256Hex(source+SALT_KEY);
		return target;
	}
	// Ctrl + Shift + X (그 다음 J)
	public static void main(String[] args) {
		String pwd = SHAUtil.getSHA("12345");
		System.out.println(pwd instanceof String);
	}
}
