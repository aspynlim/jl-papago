package com.jl.papago.test;

public class Product {
	private String[] productList = {"item1","item2","item3","item4","item5"};

	// 웹 테스트를 위한 변수값
	private int num1 = 10; 
	private int num2 = 20; 

	public int getNum1() { 
	  return num1;
	} 
	public int getNum2() { 
	  return num2;
	} 
	public String[] getProductList() {
	  return productList;
	}
}
