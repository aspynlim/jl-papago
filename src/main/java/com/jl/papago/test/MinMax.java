package com.jl.papago.test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class MinMax {
	public static void main(String[] args) {
		Random r = new Random();
		int[] points = new int[10];
		int[] duplicates = new int[11];
		
		int min = 0;
		int max = 0;
		
		List<Integer> list = new ArrayList<Integer>();
		
		for(int i=0; i<points.length; i++) {
			points[i] = r.nextInt(11);
			duplicates[points[i]]++;
			if(i == 0) {
				min = points[i];
				max = points[i];
			} else {
				if(min > points[i]) {
					min = points[i];
				}
				if(max < points[i]) {
					max = points[i];
				} 
			}
			list.add(points[i]);
		}
		System.out.println(list);
		for(int i=0; i<duplicates.length; i++){
			if(duplicates[i] > 1) {
				System.out.println("점수 : " + i + ", 중복 : " + duplicates[i]);
			}
		}
		System.out.println("max : " + max);
		System.out.println("min : " + min);
	}
}
