package com.jl.papago.test;

public class Calculation {
	private String op;
	private int num1;
	private int num2;
	
	public String getOp() {
		return op;
	}
	public void setOp(String op) {
		this.op = op;
	}
	
	public Integer getNum1() {
		return num1;
	}
	public void setNum1(Integer num1) {
		this.num1 = num1;
	}

	public Integer getNum2() {
		return num2;
	}
	public void setNum2(Integer num2) {
		this.num2 = num2;
	}
	
	@Override
	public String toString() {
		return "Calculation [op=" + op + ", num1=" + num1 + ", num2=" + num2 + "]";
	}
}
