package com.jl.papago.test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

public class PrepStExample {
	 public static void main(String[] args) {
	    try {
	       String myDriver = "com.jl.exam.mysql.Driver";
	       String myUrl = "jdbc:mysql://localhost/test";
	       Class.forName(myDriver);
	       Connection con = DriverManager.getConnection(myUrl, "root", "");
	    
	       String query = " insert into users (ui_name, ui_age, ui_blood, ui_height, ui_memo) values (?, ?, ?, ?, ?)";

	      PreparedStatement preparedStmt = con.prepareStatement(query);
	      preparedStmt.setString (1, "mike");
	      preparedStmt.setInt (2, 22);
	      preparedStmt.setString  (3, "A");
	      preparedStmt.setInt (4, 180);
	      preparedStmt.setString (5, "college student");

	      preparedStmt.execute();
	      
	      con.close();
	    } catch (Exception e) {
	      System.err.println(e.getMessage());
	    }
	}
}
