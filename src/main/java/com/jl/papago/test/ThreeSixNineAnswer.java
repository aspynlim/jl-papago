package com.jl.papago.test;

import java.util.ArrayList;
import java.util.List;

public class ThreeSixNineAnswer {
	public static void main(String[] args) {
		
		List<Object> list = new ArrayList<>();
		
		for(int i=1; i<=100; i++) {
			int num1 = i/10;
			String str = "";
			if(num1 == 3 || num1 == 6 || num1 == 9) {
				str += "짝";
			}
			
			int num2 = i%10;
			if(num2 == 3 || num2 == 6 || num2 == 9) {
				str += "짝";
			}
			
			if(str.contentEquals("")) {
				list.add(i);
			} else {
				list.add(str);
			}
		}
		
		System.out.println(list);
	}
}
