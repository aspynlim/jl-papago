package com.jl.papago.test;

import java.util.ArrayList;
import java.util.List;

public class ThreeSixNine {
	public static void main(String[] args) {
		// 1-100 출력
		// 3,6,9 짝
		int[] nums = new int[100];
		
		List<Object> list = new ArrayList<>();
		for(int i=0; i<nums.length; i++) {
			// "Integer.toString" equals to "String.valueOf"
			String num = Integer.toString(i+1);
			
			if (num.contains("3") || num.contains("6") || num.contains("9")) {		
				
			} else {
				list.add(i + 1);
			}
		}
		System.out.println(list);
	}
}

// String : replace