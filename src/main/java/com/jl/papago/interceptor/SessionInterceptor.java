package com.jl.papago.interceptor;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jl.papago.service.UserService;
import com.jl.papago.vo.UserInfoVO;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class SessionInterceptor extends HandlerInterceptorAdapter{
	
	// 개발자 모드 시작
//	@Resource 
//	private UserService us;
//	@Value("${project.mod}")
//	private String projectMod;
	// 개발자 모드 끝
	
	// provides functionality for reading and writing JSON.
	private ObjectMapper om = new ObjectMapper();
	
	@Override
	public boolean preHandle(HttpServletRequest req, HttpServletResponse res, Object handler) throws IOException {
		// 예전에 servlet 했을 때 req, res
		String uri = req.getRequestURI();
		log.info("uri => {}", uri);
		HttpSession hs = req.getSession();
		
		if(hs.getAttribute("ui") == null) {		// controller에서 ui라고 했으면
			// 개발자 모드 시작
//			if("DEV".equals(projectMod)) {
//				UserInfoVO ui = new UserInfoVO();
//				ui.setUiId("james");
//				ui.setUiPwd("12345");
//				us.doLogin(ui, hs);
//				return true;
//			}
			// 개발자 모드 끝
			String isAjax = req.getHeader("x-ajax");
			// session 끊김
			if(isAjax != null) {
				Map<String, String> map = new HashMap<>();
				map.put("msg", "Login FAILED!!!");
				res.setContentType("application/json;charset=utf-8");
				// enables you to write formatted data to an underlying Writer. 
				PrintWriter pw = res.getWriter();
				// writeObjectAsString writes it as a JSON-formatted string.
				pw.write(om.writeValueAsString(map));
				return false;
			}
			// 로그인 안 됨
			res.sendRedirect("/views/error/session-error");
			return false;
		}
		return true;
	}
}
