package com.jl.papago.app;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.jl.papago.dao.TestDAO;
import com.jl.papago.service.TestService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class execute {
	public static void main(String[] args) {
		ApplicationContext ac = new ClassPathXmlApplicationContext("test.xml");
		// testDAO에서 @Repository 없애면 에러남 (메모리 생성이 안 되서)
		log.info("TestDAO => {}", ac.getBean("testDAO"));
		
		TestDAO tdao = (TestDAO)ac.getBean("testDAO");
		TestDAO tdao1 = new TestDAO();
		// tdao와 tdao1은 같지 않다 (remember "new"!)
		
		TestService ts = (TestService)ac.getBean("testService");
		System.out.println(ts.tdao);	// 메모리 주소
		
		TestService ts1 = new TestService();	// 이게 되면 ClassPathXmlApplicationContext가 필요가 없음
		// ClassPathXmlApplicationContext가 annotation을 읽어들임
		System.out.println(ts1.tdao);	// null
	}
}
