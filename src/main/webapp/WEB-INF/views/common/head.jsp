<%@ include file ="/WEB-INF/views/common/header.jspf" %>

<link href="${cssPath}/papago.css" rel="stylesheet" />
<script src="https://kit.fontawesome.com/292ac90a40.js" crossorigin="anonymous"></script>

<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
    rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Solway&display=swap" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css">

<!-- FAVICON START -->
<link rel="icon" type="image/ico" href="${faviconPath}/favicon.ico?"/>
<link rel="apple-touch-icon" sizes="180x180" href="${faviconPath}/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="${faviconPath}/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="${faviconPath}/favicon-16x16.png">
<link rel="manifest" href="${faviconPath}/site.webmanifest">
<link rel="mask-icon" href="${faviconPath}/safari-pinned-tab.svg" color="#5bbad5">
<meta name="msapplication-TileColor" content="#da532c">
<meta name="theme-color" content="#ffffff">
<!-- FAVICON END -->

<!-- PrismJS START -->
<link href="${prismPath}/prism.css" rel="stylesheet" />
<script src="${prismPath}/prism.js"></script>
<!-- PrismJS END -->

<script src="${jsPath}/common.js"></script>
<script src="${jsPath}/ajax.js"></script>
<!-- <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>  -->
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9/dist/sweetalert2.all.min.js"></script>
<script
	src="https://code.jquery.com/jquery-3.4.1.min.js"
	integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
	crossorigin="anonymous"></script>