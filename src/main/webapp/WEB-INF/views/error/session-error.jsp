<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<html>
<head>
	<title>Home</title>
	<jsp:include page="/WEB-INF/views/common/head.jsp"></jsp:include>
</head>
<body class="main-div">
	<div id="error-bg">
		<div id="error-div">
			<div id="ed-top">
				<h1 id="error-title" class="animated rubberBand">Wait...!</h1>
			</div>
			<div id="ed-md">
				<p>You're trying to access to user-only contents.</p>
				<p>Please log in to continue.</p>
			</div>
			<div id="ed-btm">
				<button id="error-button" class="animated swing" onclick="goPage('/user/login')"><span>Login</span></button>
			</div>
		</div>
		<div class="photo-credit">
			<span>Photo by <a href="https://unsplash.com/@jodaarba" target="_blank">Jose Aragones</a> on <a href="https://unsplash.com/" target="_blank">Unsplash</a></span>
		</div>
	</div>
</body>
</html>