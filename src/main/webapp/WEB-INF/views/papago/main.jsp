<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<title>Main</title>
</head>
<body>
    <div id="main-div">
        <h1 id="main-title" class="animated slideInLeft">Welcome, ${ ui.getUiName() } !</h1>
        <div id="main-features">
            <div id="ft-title">
                <p>Translate</p>
            </div>
            <div id="ft-contents">
                <p>Translate texts</p>
            </div>
            <div id="fr-title">
                <p>Results</p>
            </div>
            <div id="fr-contents">
                <p>Search results</p>
                <p>Delete results</p>
            </div>
            <div id="fs-title">
                <p>Stats</p>
            </div>
            <div id="fs-contents">
                <p>Sort requests</p>
            </div>                
        </div>
		<div id="main-article">
			<p>Check out <a href="http://bit.ly/papago-ver1" target="_blank">this PPT</a> for detailed explanation of this project.</p>
		</div>
    </div>
</body>

<!--  
<pre class="line-numbers">
    <code class="language-java match-braces">
test
    </code>
</pre>
<script type="text/plain" class="language-markup  line-numbers">
<dependency>
  <groupId>org.springframework</groupId>
  <artifactId>spring-context</artifactId>
  <version>${org.springframework-version}</version>
  <exclusions>
    <exclusion>
      <groupId>commons-logging</groupId>
      <artifactId>commons-logging</artifactId>
    </exclusion>
  </exclusions>
</dependency>
</script>
-->