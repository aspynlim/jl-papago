<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>LOGOUT</title>
<jsp:include page="/WEB-INF/views/common/head.jsp"></jsp:include>
</head>
<body>
<div id="result">
	<h3>When you log out, you will</h3>
	<ol>
		<li><h4>lose an access to see Papago contents.</h4></li>
		<li><h4>be directed to the home page.</h4></li>
	</ol>
	<button class="index-buttons"><span>LOGOUT</span></button>
</div>
</body>
<script>
$(document).ready(function(){
	$('button').on('click', function(){
		$.ajax({
			url: '/logout',
			method: 'POST',
			success: function(res){
				if(res.result == 'true'){
					Swal.fire({
						position: 'top',
						icon: 'success',
						title: 'Congratulations!',
						text: 'You successfully logged out !'
					}).then(() => {
						location.href = '/';
					})
				}
			},
			error: function(res){
				console.log(res);
			}
		});	
	})
});
</script>
</html>