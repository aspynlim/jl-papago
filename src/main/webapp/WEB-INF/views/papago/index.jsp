<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <jsp:include page="/WEB-INF/views/common/head.jsp"></jsp:include>
</head>
<body class="pages">
    <nav>
        <ul id="navbar-left">
            <li id="logo-li">
                <img src="../../../resources/img/papago.svg" alt="papago logo"/>
            </li>
            <li data-page="papago" class="nav-menu">
                 <a href="?page=papago"><span class="nav-text-links">Translate</span></a>
            </li>
            <li data-page="results" class="nav-menu">
                <a href="?page=results"><span class="nav-text-links">Results</span></a>
            </li>
            <li data-page="stats" class="nav-menu">
                <a href="?page=stats"><span class="nav-text-links">Stats</span></a>
            </li>
        </ul>
        <ul id="navbar-right">
            <li class="active nav-menu" data-page="main">
                <a href="?page=main"><span class="nav-text-links">Main</span></a>
            </li>
            <li>
                <span class="nav-text-links nav-menu" onclick="goIndexPage('/')">Home</span>
            </li>
            <li class="nav-menu">
               <a href="?page=logout"> <span class="nav-text-links" >logout</span></a>
            </li>
            <li id="nav-dropdown-trigger">
                <i id="nav-hamburger-icon" class="material-icons">menu</i>
                <i id="nav-clear-icon" class="material-icons">clear</i>
            	<div id="nav-dropdown-div">
	            	<a href="?page=papago"><span class="nav-text-links nav-dropdown-menu">Translate</span></a>
	            	<a href="?page=results"><span class="nav-text-links nav-dropdown-menu">Results</span></a>
	            	<a href="?page=stats"><span class="nav-text-links nav-dropdown-menu">Stats</span></a>
	            	<a href="?page=main"><span class="nav-text-links nav-dropdown-menu">Main</span></a>
	            	<a href="#"  onclick="goIndexPage('/')"><span class="nav-text-links nav-menu nav-dropdown-menu">Home</span></a>
	            	<a href="?page=logout"> <span class="nav-text-links nav-dropdown-menu" >logout</span></a>
            	</div>
            </li>
        </ul>
    </nav>	
    <div class="container">
        <c:if test="${empty param.page}">
            <jsp:include page="/WEB-INF/views/papago/main.jsp"></jsp:include>
        </c:if>
        <c:if test="${not empty param.page}">
            <jsp:include page="/WEB-INF/views/papago/${param.page}.jsp"></jsp:include>
        </c:if>
    </div>
	<footer>
		2020 <i class="material-icons">copyright</i>  <a id="footer-linkedin" href="https://www.linkedin.com/in/hyejunglim/" target="_blank">Hyejung Lim</a>
	</footer>
<script>
$(document).ready(function(){
	var pPage = '${param.page}';
	if(!pPage){
		pPage = 'main';
	}
	$('li[data-page]').removeClass('active');
	$('li[data-page=' +pPage + ']').addClass('active');
	$('li[data-page]').on('click', function(){
		var page = this.getAttribute('data-page');
		location.href='/?page=' + page;
	})
	
	$('#nav-dropdown-trigger').click(function() {
		$("#nav-dropdown-div").toggle();
		$(".nav-dropdown-menu").toggle();
        $("#nav-hamburger-icon").toggle();
        $("#nav-clear-icon").toggle();
	});
})
</script>
</body>
</html>