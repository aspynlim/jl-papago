<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<title>Results</title>
</head>
<body class="pages">	
	<div id="results-div">
		<h1 id="results-title" class="animated lightSpeedIn">Papago Results !</h1>
		<div id="search-and-delete">
			<div id="search-div">
				<div id="option-1">
					<label for="piSource">Source Lang</label>
					<input type="checkbox" id="piSource" class="checkOne" value="pi_source" name="search" />
				</div>
				<div id="option-2">
					<label for="piTarget">Target Lang</label>
					<input type="checkbox" id="piTarget" class="checkOne" value="pi_target" name="search" />
				</div>
				<div id="option-3">
					<label for="piText">Source Text</label>
					<input type="checkbox" id="piText" class="checkOne" value="pi_text" name="search" />
				</div>
				<div id="option-4">
					<label for="piResult">Target Text</label>
					<input type="checkbox" id="piResult" class="checkOne" value="pi_result" name="search" />
				</div>
				<div id="option-all">
					<label>SELECT ALL</label>
					<input type="checkbox" id="checkAll" onclick="selectAll()" />
				</div>
				<div id="search-box-div">
					<input type="text" id="searchStr">
					<button id="search-button">SEARCH</button>
				</div>
				<button id="delete-button" onclick="deleteUsers()">DELETE</button>
			</div>
			<button id="delete-button-large-screen" onclick="deleteUsers()">DELETE</button>
		</div>
		<table data-id="all">
			<thead>
				<tr>
					<th>Delete</th>
					<th data-order="pi_num desc">Record # ▼</th>
					<th data-order="pi_source desc">Source Lang ▼</th>
					<th data-order="pi_target desc">Target Lang ▼</th>
					<th data-order="pi_text desc">Source Text ▼</th>
					<th data-order="pi_result desc">Target Text ▼</th>
					<th data-order="pi_cnt desc">Count ▼</th>
				</tr>
			</thead>
			<tbody id="tBody"></tbody>
		</table>
	</div>
<script src="../../../resources/js/results.js"></script>
</body>
</html>