<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<title>Translate</title>
</head>
<body class="pages">
    <div id="papago-div">
        <div id="papago-before-top">
            <select id="source" class="select-lang">
                <option value="ko">Korean</option>
                <option value="en">English</option>
                <option value="ja">Japanese</option>
                <option value="zh-CN">Chinese(Simplified)</option>
                <option value="zh-TW">Chinese(Traditional)</option>
                <option value="es">Spanish</option>
                <option value="fr">French</option>
                <option value="de">German</option>
                <option value="ru">Russian</option>
                <option value="it">Italian</option>
                <option value="vi">Vietnamese</option>
                <option value="th">Thai</option>
                <option value="id">Indonesian</option>
            </select>
        </div>
        <div id="papago-before-bottom">
            <textarea id="text" placeholder="Type texts to Translate."></textarea>
        </div>
        <div id="papago-after-top">
            <select id="target" class="select-lang">
                <option value="en">English</option>
                <option value="ko">Korean</option>
                <option value="ja">Japanese</option>
                <option value="zh-CN">Chinese(Simplified)</option>
                <option value="zh-TW">Chinese(Traditional)</option>
                <option value="es">Spanish</option>
                <option value="fr">French</option>
                <option value="de">German</option>
                <option value="ru">Russian</option>
                <option value="it">Italian</option>
                <option value="vi">Vietnamese</option>
                <option value="th">Thai</option>
                <option value="id">Indonesian</option>
            </select>
        </div>
        <div id="papago-after-bottom">
            <textarea id="result" type="text"></textarea>
        </div>
        <div id="papago-button-div">
            <button id="papago-button">Translate</button>
        </div>
        <div id="papago-info">
        	<p> Korean<br>- English, Japanese, Chinese(Simplified), Chinese(Traditional), Spanish, French, German, Russian, Italian, Vietnamese, Thai, Indonesian</p>
            <p>           
            Chinese(Simplified)<br> - Chinese(Traditional), Japanese</p>
            <p>
            Chinese(Traditional)<br> - Japanese</p>
            <p> English<br>- Japanese, Chinese(Simplified), Chinese(Traditional), French</p>
        </div>
    </div>
    <script src="../../../resources/js/papago.js"></script>
</body>
</html>