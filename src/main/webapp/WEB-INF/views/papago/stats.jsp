<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<title>Stats</title>
</head>
<body class="pages">	
	<div id="stats-div">
		<h1 class="animated lightSpeedIn">Stats list</h1>
		<button type="button" data-target="credat">By Date</button>
		<button type="button" data-target="user">By User</button>
		<button type="button" data-target="all">All</button><br><br>
		<table data-id="all" id="all-div">
			<thead>
				<tr>
					<th data-order="ps.ps_num desc">Record # ▼</th>
					<th data-order="ui.ui_num desc">Request User # ▼</th>
					<th data-order="ps.ui_num desc">Request User ID ▼</th>
					<th data-order="pi_num desc">Translation # ▼</th>
					<th data-order="ps.credat desc">Date ▼</th>
				</tr>
			</thead>
			<tbody id="tBody"></tbody>
		</table>
		<table data-id="credat" style="display:none;">
			<thead>
				<tr>
					<th data-order="ps.credat desc">By Date ▼</th>
					<th data-order="cnt desc">By # of Searches ▼</th>
				</tr>
			</thead>
			<tbody id="tBody-credat"></tbody>
		</table>
		<table data-id="user" style="display:none;">
			<thead>
				<tr>
					<th data-order="ps.ui_num desc">Request User # ▼</th>
					<th data-order="ui.ui_id desc">Request User ID ▼</th>
					<th data-order="cnt desc">By # of Searches ▼</th>
				</tr>
			</thead>
			<tbody id="tBody-user"></tbody>
		</table>
	</div>
<script src="../../../resources/js/stats.js"></script>
</body>
</html>