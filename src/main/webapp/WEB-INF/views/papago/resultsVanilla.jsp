<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>List</title>
<jsp:include page="/WEB-INF/views/common/head.jsp"></jsp:include>
</head>
<body class="pages">
	<nav>
		<ul id="navbar-left">
			<li id="logo-li"><img src="../../../resources/img/papago.svg" alt="papago logo"/></li>
			<li><a href="#" target="_blank">Stats</a></li>
			<li><a href="https://papago.naver.net/website?locale=en" target="_blank">Web Translator</a></li>
			<li><a href="https://dict.naver.com/" target="_blank">Dictionary</a></li>
		</ul>
		<ul id="navbar-right">
			<li><i class="material-icons main-icons" data-toggle="tooltip" data-placement="top" title="Small Window Translator">desktop_windows</i></li>
			<li><i class="material-icons main-icons" data-toggle="tooltip" data-placement="top" title="Favorites">star</i></li>
			<li><i class="material-icons main-icons" data-toggle="tooltip" data-placement="top" title="Setting">settings_applications</i></li>
			<li><a href="#" target="_blank">login</a></li>
			<li><i class="material-icons">menu</i></li>
		</ul>
	</nav>	
	<div id="results-div">
		<h1>Papago Results</h1>
		<div class="search">
			<label for="uiNum">번호</label> <input type="checkbox" id="uiNum" value="ui_num" name="search" />
			<label for="uiName">이름</label> <input type="checkbox" id="uiName" value="ui_name" name="search" />
			<label for="uiId">ID</label> <input type="checkbox" id="uiId" value="ui_id" name="search" />
			<input type="text" id="searchStr"><button>검색</button>
		</div><br>
		<table class="table">
		<thead class="thead-dark">
			<tr>
			<th scope="col">Delete</th>
			<th scope="col">#</th>
			<th scope="col">Source</th>
			<th scope="col">Target</th>
			<th scope="col">Text</th>
			<th scope="col">Result</th>
			<th scope="col">Count</th>
			<th scope="col">Edit</th>
			</tr>
		</thead>
		<tbody id="tBody"></tbody>
		</table>
		<button type="button" onclick="goPage('/papago/papago')">Translate</button>
		<button type="button" onclick="deleteUsers()">삭제</button>
	</div>
	<footer>
		2019 <i class="material-icons">copyright</i> <a href="https://www.navercorp.com/ko" target="_blank">NAVER.corp</a>
	</footer>
<script>
function deleteUsers(){
	var checks = document.querySelectorAll('input[name=remove]:checked');
	if(checks.length<1){
		alert('Please select more than one item to delete!');
		return;
	}

	var piNums = '';
	for(var i=0; i<checks.length; i++){
		piNums += 'piNums=' + [checks[i].value] + '&';
	}
	alert(piNums);
	
	var xhr2 = new XMLHttpRequest();
	xhr2.open('DELETE', '/papago?' + uiNums);
	xhr2.setRequestHeader('Content-Type','application/json');
	xhr2.onreadystatechange = function(){
		if(xhr2.readyState == 4 && xhr2.status == 200){
			var res = JSON.parse(xhr2.responseText);
			console.log(res);
	 	 	if(res.cnt != 0){
	 	 		 location.href = '/views/papago/results';
	 	 	}
		}
	}
	xhr2.send(uiNums);
}

function getPapagoList(param){
	var xhr = new XMLHttpRequest();
	xhr.open('GET','/papago?' + param);
	xhr.setRequestHeader('Content-Type', 'application/json');
	xhr.onreadystatechange = function(){
		if(xhr.readyState == 4 && xhr.status == 200){
			var list = JSON.parse(xhr.responseText);
			console.log(list);
			var tBody = document.getElementById('tBody');
			var html = '';
			for(var item of list){
				html += '<tr>';
				html += '<td><input type="checkbox" name="remove" value="' + item.psNum + '"></td>';
				html += '<td>' + item.piNum + '</td>';
				html += '<td>' + item.piSource + '</td>';
				html += '<td>' + item.piTarget + '</td>';
				html += '<td>' + item.piText + '</td>';
				html += '<td>' + item.piResult + '</td>';
				html += '<td>' + item.piCnt + '</td>';
				html += '<td><a href="/views/papago/view?piNum=' + item.piNum +'"><button>' + item.piNum + '</button></a></td>';
				html += '</tr>';
			}
			tBody.innerHTML = html;
		}
	}
	xhr.send();
}
window.onload = function(){
	getPapagoList('');
}
</script>
</body>
</html>