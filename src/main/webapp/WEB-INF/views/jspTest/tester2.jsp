<%@ page language="java" contentType="text/html; charset=UTF-8"    pageEncoding="UTF-8"%>   
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
<head><meta charset"=UTF-8"><title>Insert title here</title></head>
<body>
<h3>&lt;c:choose&gt;</h3>
<form>
	<select name="sel">
		<option>-</option>
		<option ${param.sel=='a'? 'selected':''}>a</option>
		<option ${param.sel=='b'? 'selected':''}>b</option>
		<option ${param.sel=='c'? 'selected':''}>c</option>
		<option ${param.sel=='d'? 'selected':''}>d</option>
	</select>
	<input type="submit" value="선택">
</form>

<%
String selection = request.getParameter("sel");

if (selection != null){
%>

<c:choose>
	<c:when test="${param.sel == 'a' || param.sel == 'b' || param.sel == 'c'}">
	
	<% 
		out.println(selection + " 를 선택");
	%>	
	
	</c:when>
	<c:otherwise>
	
	<%
    out.println("a,b,c 중에 하나를 선택해주세요.");
	%>	
	
	</c:otherwise>
</c:choose>

<%
} else {
	out.println("선택해 주세요.");
}
%>
</body>
</html>