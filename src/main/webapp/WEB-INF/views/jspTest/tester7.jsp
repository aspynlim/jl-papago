<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Request</title>
<style>
	.title-div {
		border-bottom: 1px solid gray;
	}
	h1{
		text-align: center;
	}
	#form-div {
		margin-top: 2em;
		display: flex;
  		justify-content: center;
	}
	#buttons-td {
		text-align: center;
	}
	#result-div {
		margin-top: 2em;
		display: flex;
  		justify-content: center;		
	}
</style>
</head>
<body>
	<div class="title-div">
		<h1>request 테스트 폼</h1>
	</div>
	<div id="form-div">
		<table border="1">
			<tr>
				<td>이름</td>
				<td><input type="text" name="name" id="name" /></td>
			</tr>
			<tr>
				<td>직업</td>
				<td>
					<select id="occupation">
						<option value="무직" selected>무직</option>
						<option value="엔지니어">엔지니어</option>
						<option value="농부">농부</option>
						<option value="가수">가수</option>
					</select>
				</td>
			</tr>
			<tr>
				<td>관심분야</td>
				<td>
					<input type="checkbox" id="politics" value="정치" name="interest" />정치
					<input type="checkbox" id="society" value="사회" name="interest" />사회
					<input type="checkbox" id="it" value="정보통신" name="interest" />정보통신<br>
				</td>
			</tr>
			<tr  id="buttons-td">
				<td  colspan="2">
					<button id="submit-btn" onclick="submit()">확인</button>
					<button>취소</button>
				</td>
			</tr>
		</table>
	</div>
	<div class="title-div">
		<h1>request 테스트 결과 - 1</h1>
	</div>
	<div id="result-div">
		<table border="1">
			<tr>
				<td>이름</td>
				<td id="rs_name"></td>
			</tr>
			<tr>
				<td>직업</td>
				<td id="rs_occupation">	</td>
			</tr>
			<tr>
				<td>관심분야</td>
				<td id="rs_interests"></td>
			</tr>
		</table>
	</div>
<script>
function submit() {
	var param = {
		name : document.querySelector('#name').value,
		occupation : document.querySelector('#occupation').value,
		interests: document.querySelectorAll("[name=interest]:checked")
	}
	var node = document.createElement('p');
	var textnode = document.createTextNode('' + param.name + '');
	node.appendChild(textnode);
	document.getElementById('rs_name').appendChild(node);
	
	var node2 = document.createElement('p');
	var textnode2 = document.createTextNode('' + param.occupation + '');
	node2.appendChild(textnode2);
	document.getElementById('rs_occupation').appendChild(node2);
	
	for(var i=0; i<param.interests.length; i++){
		document.getElementById('rs_interests').appendChild(document.createElement('p').appendChild(document.createTextNode('' + param.interests[i].value + ' ')));
	}
}
</script>
</body>
</html>