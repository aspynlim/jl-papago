<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<style type="text/css">
	.title-div {
		border-bottom: 1px solid gray;
	}
	h1{
		text-align: center;
	}
</style>
</head>
<body>
	<div class="title-div">
		<h1>session 예제</h1>
	</div>
	<div id="welcom-div">
		<h1 id="welcome-msg"># ${ ui.getUiName() } 님 환영합니다 !!!!</h1>
	</div>
</body>
</html>