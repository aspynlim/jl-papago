<%@ page language="java" contentType="text/html; charset=UTF-8"    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
<head>
	<meta charset"=UTF-8">
	<title>상품 목록</title>
</head>

<body>
<div align="center">
	<H2>EL 예제-상품 목록</H2><HR>

	<form method="post" action="tester4">
		<jsp:useBean id="product" class="com.jl.papago.test.Product" scope="session"/>

		<select name="sel">
			<% 
			for(String item : product.getProductList()) {  
				out.println("<option>"+ item+"</option>");  
			} 
			%> 
		</select> 
	
		<input type="submit" value="선택"/>
	</form>
</div>
</body>
</html>