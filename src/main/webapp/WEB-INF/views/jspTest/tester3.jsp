<%@ page import="java.util.SortedMap" %>
<%@ page import="java.util.TreeMap" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"    pageEncoding="UTF-8"%>   
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%
SortedMap<Integer, String> map = new TreeMap<Integer, String>();
	map.put(1, "1");
	map.put(2, "2");
	map.put(3, "3");
	map.put(4, "4");
	map.put(5, "5");
	map.put(6, "6");
	request.setAttribute("eachMap", map);
%>

<!DOCTYPE html>
<html>
<head>
	<meta charset"=UTF-8">
	<title>forEach & forTokens</title>
</head>
<body>

	<c:forEach var="list" items="${eachMap}" varStatus="opt" >
		index : ${opt.index} / count : ${list.value} <br/>
		name : tester${opt.index} <br/>
		email : test${opt.index}@test.net <br/>
		<hr>
	</c:forEach>

	<c:set var="token" value="홍길동, 011-211-0090, 서울" />
	<c:forTokens var="list" items="${token}" delims=",">
		${list} <br/>
	</c:forTokens>
</body>
</html>