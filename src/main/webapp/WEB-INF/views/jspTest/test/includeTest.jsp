<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>include 지시어 테스트</title>
<style>
	body {
		margin: 0;
		padding: 0;
	}
	#include-div {
		display:  grid;
		grid-template-columns: repeat(12,1fr);
		grid-template-rows: repeat(6, 50px) 1fr;
	}
	#title-div {
		grid-column: 1/13;
		grid-row: 1/3;
		border-bottom: 0.1em solid black;
	}
	#title {
		text-align: center;
	}
	#subtitles-div {
		grid-column: 1/13;
		grid-row: 3/4;
	}
	#subtitles {
		text-align: center;
		font-weight: bold;
	}
	#contents-div-left {
		grid-column: 2/7;
		grid-row: 4/7;
		grid-template-rows: repeat(3,1fr);
	}
	#left-title{
		grid-row: 1/2;
		border-bottom: 0.1em solid gray;
	}
	#left-contents{
		grid-row: 2/4;
	}
	#contents-div-right {
		grid-column: 8/12;
		grid-row: 4/7;
		grid-template-rows: repeat(3,1fr);
	}
	#right-title{
		grid-row: 1/2;
		border-bottom: 0.1em solid gray;
	}
	#right-contents{
		grid-row: 2/4;
	}
</style>
</head>
<body>
   <div id="include-div">
   		<div id="title-div">
   			<h1 id="title">include 지시어 테스트</h1>
   		</div>
   		<div id="subtitles-div">
   			<p id="subtitles">[게임] [쇼핑] [뉴스]</p>
   		</div>
   		<div id="contents-div-left">
   			<div id="left-title">
   				<p>[최신 뉴스]</p>
   			</div>
   			<div id="left-contents">
   				<p>2013.00.00 : 자바 웹 프로그래밍 전면 개정판 출간 !!!</p>
   				<p>2013.00.00 : 스프링프레임워크 적용 확산.</p>
   			</div>
   		</div>
   		<div id="contents-div-right">
   			<div id="right-title">
   				<p>[쇼핑정보] 최신 인기 상품 정보입니다.</p>
   			</div>
   			<div id="right-contents">
   			   	<p>1. 최신 스마트폰</p>
   			   	<p>2. 10.1인치 최신 태플랫 PC</p>
   			</div>
   		</div>
   </div>
</body>
</html>