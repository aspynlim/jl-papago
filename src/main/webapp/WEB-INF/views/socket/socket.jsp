<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<button id="open">연결</button>
	<div id="rDiv" style="display: none;">
		<input type="text" id="msg"><button id="send">전송</button>
	</div>
	<div id="history" style="display: none;">
	</div>
<script>
function qs(id){
	return document.querySelector(id);
}
var ws;
window.onload = function(){
	document.querySelector('button').onclick = function(){
// 		var url = 'ws://192.168.0.20/ws/chat';
		var url = 'ws://192.168.0.20/wst';
		ws = new WebSocket(url);	// 연결됨!
		ws.onopen = function(evt){
			if(evt && evt.isTrusted){
				qs('#open').style.display = 'none';
				qs('#rDiv').style.display = '';
				qs('#history').style.display = '';
				qs('#send').onclick = function(){
					var msg = qs('#msg').value;
					ws.send(msg);
				}
			}
		}
		ws.onmessage = function(evt){
			console.log(evt.data);
			// 일반적인 string 대신 JSON 형태로 하는 것이 좋음
			var html = '<p>' + evt.data + '</p>';
			qs('#history').innerHTML += html;
		}
	}
}
</script>
</body>
</html>