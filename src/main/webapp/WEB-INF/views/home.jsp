<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<html>
<head>
	<title>Hyejung Lim's Portfolio</title>
	<jsp:include page="/WEB-INF/views/common/head.jsp"></jsp:include>
</head>
<body class="main-div">
	<div id="index-bg">
		<div id="index-social-media">
			<div id="div-linkedin">
				<a href="https://www.linkedin.com/in/hyejunglim/" target="_blank"><img src="../../resources/img/logo-linkedin.png" alt="LinkedIn Logo"/></a>
			</div>
			<div id="div-medium">
				<a href="https://medium.com/@cmdlhz" target="_blank"><img src="../../resources/img/logo-medium.svg" alt="Medium Logo"/></a>
			</div>
			<div id="div-cmdlhz">
				<img src="../../resources/fv/favicon-32x32.png" alt="Favicon Logo"/>
			</div>
			<div id="div-stackoverflow">
				<a href="https://stackoverflow.com/users/10021131/jen-lim" target="_blank"><img src="../../resources/img/logo-stackoverflow.svg" alt="StackOverflow Logo"/></a>
			</div>
			<div id="div-twitter">
				<a href="https://twitter.com/cmdlhz" target="_blank"><img src="../../resources/img/logo-twitter.svg" alt="Twitter Logo"/></a>
			</div>
		</div>
		<div id="index-div">
			<div id="id-top">
				<h1 id="index-title" class="animated bounceInDown">Hyejung Lim's Portfolio</h1>
			</div>
			<div id="id-md">
				<table id="id-imgs">
					<thead>
						<tr>
							<th class="col"><i class="material-icons">directions_boat</i> Category</th>
							<th class="col"><i class="material-icons">directions_boat</i> Tech Specs</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<th class="row">Frontend</th>
							<td>HTML, Sass, JavaScript</td>
						</tr>
						<tr>
							<th class="row">Backend</th>
							<td>Java 8 / Spring 4</td>
						</tr>
						<tr>
							<th class="row">Server</th>
							<td>Tomcat, Jenkins</td>
						</tr>
						<tr>
							<th class="row">Cloud Computing Platform</th>
							<td>AWS EC2 & RDS</td>
						</tr>
						<tr>
							<th class="row">Database</th>
							<td>MariaDB</td>
						</tr>
						<tr>
							<th class="row">Big Data</th>
							<td>Hadoop(MapReduce, Hive, Flume, Spark), R, Python</td>
						</tr>
						<tr>
							<th class="row">DevOps Lifecycle Tool</th>
							<td>Gitlab</td>
						</tr>
						<tr>
							<th class="row">Development Tools</th>
							<td>Eclipse, Visual Studio, VMWare, CentOS, RStudio, Jupyter</td>
						</tr>
						<tr>
							<th class="row">Design Tool</th>
							<td>Figma</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div id="id-btm">
				<button class="index-buttons" onclick="goPage('/user/login')"><span>Java Project</span></button>
				<button class="index-buttons" id="bigdata-button"><span>BigData Project</span></button>
			</div>
		</div>
		<div class="photo-credit">
			<span>Photo by <a href="https://unsplash.com/@joshuaearle" target="_blank">Joshua Earle</a> on <a href="https://unsplash.com/" target="_blank">Unsplash</a></span>
		</div>
	</div>
	
<script>
var favicon = document.getElementById('div-cmdlhz');
favicon.addEventListener('click', function(){
	Swal.fire({
		position: 'top',
		icon: 'info',
		title: 'Not Yet Prepared',
		text: 'Identity Branding & Style Guide for Hyejung(Jen) Lim will be added soon.'
	})
})

var bigdata = document.getElementById('bigdata-button');
bigdata.addEventListener('click', function(){
	Swal.fire({
		position: 'top',
		icon: 'info',
		title: 'In Progress',
		text: 'Currently working on analyzing major infectious diseases in Korea'
	})
})
</script>
</body>
</html>