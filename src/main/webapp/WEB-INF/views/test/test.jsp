<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Insert title here</title>
<%-- 	<jsp:include page="/WEB-INF/views/common/head.jsp"></jsp:include> --%>
	<script
	src="https://code.jquery.com/jquery-3.4.1.min.js"
	integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
	crossorigin="anonymous"></script>
</head>
<body>
	<input type="radio" name="name" value="제인">제인<br>
	<input type="radio" name="name" value="카일">카일<br>
	<input type="radio" name="name" value="가을">가을<br>
	<input type="radio" name="name" value="겨울">겨울
</body>
<%-- <c:if test="${name == '제인'}">checked</c:if> --%>

<script>
	// document.ready로 감싸는 것이 좋음
	var name = '${name}';
	console.log(name);
	
	$('input[value=' + name + ']').attr('checked', true);	// checked=true
	console.log($('input[type=radio]').length);
	
	$('input[type=radio]').on('click', function(){
		console.log(this.checked);
		console.log(this.value);
		console.log(this);
		
		var param = 'name=' + this.value;
		$.ajax({
			url : '/test/name',
			method: 'POST',
			data: param,
			success: function(res){
				alert(res.msg);
			}
		});
	});
</script>
<%-- <c:if test="${name == null}"> --%>
<!-- 	<script> -->
<!-- // 	$.ajax({ -->
<!-- // 		url: '/test/ajax', -->
<!-- // 		method: 'GET', -->
<!-- // 		success: function(res){ -->
<!-- // // 			$('#name').text(res); -->
<!-- // 		}, -->
<!-- // 		error: function(res){ -->
<!-- // 			console.log(res); -->
<!-- // 		} -->
<!-- // 	}) -->
<!-- 	</script> -->
<%-- </c:if> --%>
</html>

<!-- page < request < session < application(서버 전체) scopes -->
<!-- 
	내가 다른 checkbox를 선택해도 server는 모름
	* application scope 변경
	* static 영역 만들고 thread 관리해도 되지만 안정성이... 98.8%.. 근데 0.2%가 큰 문제를 일으킬수도..
		=> mybatis 부분만 변경되게 할 때 이런 형태로 씀
	
	[주의] application scope 함부로 만지지 마세요!
 -->