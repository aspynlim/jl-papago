<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
	<script
	src="https://code.jquery.com/jquery-3.4.1.min.js"
	integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
	crossorigin="anonymous"></script>
</head>
<body>
	<textarea id="test">	</textarea><br>
	ID : <input type="text" id="id" /><br>
	PWD : <input type="text" id="msg" /><br>
	<button>SEND</button>

<script type="text/javascript"> // 예전 방법 : (Websocket 이전)
$(document).ready(function(){
	$('button').on('click', function(){
		if($('#id').val().trim().length < 2){
			alert('Please enter more than 2 characters.');
			$('#id').focus();
			return;
		}
		
		if($('#msg').val().trim().length < 1){
			alert('Please enter more than 1 characters.');
			$('#msg').focus();
			return;
		}
		var param = "id=" + $('#id').val() + "&msg=" + $('#msg').val();
		$.ajax({
			url: '/msg',
			method: 'POST',
			data: param,
			success: function(res){
				var text = '';
				for(var str of res){
					var id = str.split('$:v')[0];
					var msg = str.split('$:v')[1];
					text += id + ':' + msg;
				}
				$('#test').val(text);
			}
		})
	});
	
	setInterval(function(){
		$.ajax({
			url: '/msg',
			method: 'GET',
			success: function(res){
				var text = '';
				for(var str of res){
					var id = str.split('$:v')[0];
					var msg = str.split('$:v')[1];
					text += id + ':' + msg;
				}
				$('#test').val(text);
			}
		})
	}, 1000)
})
</script>
</body>
</html>