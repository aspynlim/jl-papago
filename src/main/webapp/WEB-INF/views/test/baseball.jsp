<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Baseball</title>
	<script
	src="https://code.jquery.com/jquery-3.4.1.min.js"
	integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
	crossorigin="anonymous"></script>
</head>
<body>
<button id="start">START</button>
<div id="rDiv"></div>
<div id="result"></div>
<script>
$(document).ready(function(){
	$('#start').on('click', function(){
		$.ajax({
			url: '/init',
			method: 'GET',
			success: function(res){
				if(res == "complete"){
					console.log('GAME START!');
					$('#start').css('display', 'none');
					var html = '<input type="text" id="source" /><button id="game">전송</button>';
					// .html = innerHTML : innerText는 tag를 못 넣음
					$('#rDiv').html(html);
					$('#game').on('click', function(){
						var source = $('#source').val();
						
						if(isNaN(parseInt(source))){
							alert('please enter a number.');
							return false;							
						}
						if(source.trim().length < 3){
							alert('please enter 3 digits.');
							return false;
						}
						var param = "source=" + source;
						
						$.ajax({
							url: '/game',
							// init이 포스트가 더 맞긴 함. 이건 'get'
							method: 'POST',
							data: param,
							success: function(res){
								var resultHTML = $('#result').html();
								resultHTML += source + " : " + JSON.stringify(res) + '<br>';
								$('#result').html(resultHTML);
							}							
						})
					})
				}
			}
		})
	})
})
</script>
</body>
</html>