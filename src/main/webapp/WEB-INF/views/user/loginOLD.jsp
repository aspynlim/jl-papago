<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<jsp:include page="/WEB-INF/views/common/head.jsp"></jsp:include>
<title>Insert title here</title>
</head>
<body class="pages row-fixed">
    <nav>
        <ul id="navbar-left">
            <li id="logo-li"><img src="../../../resources/img/papago.svg" alt="papago logo"/></li>
            <li><a href="#" target="_blank">Stats</a></li>
            <li><a href="https://papago.naver.net/website?locale=en" target="_blank">Web Translator</a></li>
            <li><a href="https://dict.naver.com/" target="_blank">Dictionary</a></li>
        </ul>
        <ul id="navbar-right">
            <li><i class="material-icons main-icons" data-toggle="tooltip" data-placement="top" title="Small Window Translator">desktop_windows</i></li>
            <li><i class="material-icons main-icons" data-toggle="tooltip" data-placement="top" title="Favorites">star</i></li>
            <li><i class="material-icons main-icons" data-toggle="tooltip" data-placement="top" title="Setting">settings_applications</i></li>
            <li><a href="#" target="_blank">login</a></li>
            <li><i class="material-icons">menu</i></li>
        </ul>
    </nav>
    <div id="login-title">
        <h1>Welcome !</h1>
    </div>
    <article id="login-main">
        <div id="lm-top">
			<p>Log into Papago</p>
        </div>
        <div id="lm-md">
			<div id="lm-md-id">
                <label for="uiId" id="title-id">
                    <i class="material-icons input_icon_form">person</i>
                    ID
                </label>
                <input type="text" id="uiId" maxlength="10" placeholder="Your Username">
            </div>
            <div id="lm-md-pwd">
                <label for="uiPwd" id="title-pwd">
                    <i class="material-icons input_icon_form">lock</i>
                    Password
                </label>
                <input type="password" id="uiPwd" maxlength="10" placeholder="Your Password">
            </div>
			<div id="lm-md-button">
                <button id="button-login">Login</button>
            </div>
        
        </div>
        <div id="lm-divider">
            <p>OR</p>
        </div>
        <div id="lm-btm">
            <div id="button-kakaotalk" class="social-icons">
                <div class="si-icons">
<!--                     <img src="../../../resources/img/kakaotalk.svg" alt="kakaotalk"> -->
                </div>
                <div class="si-texts">
                    <p><a href="#">Sign up with Kakaotalk !</a></p>
                </div>
            </div>
            <div id="button-facebook" class="social-icons">
                <div class="si-icons">
<!--                     <i class="fab fa-facebook fa-2x"></i> -->
                </div>
                <div class="si-texts">
                    <p><a href="#">Sign up with Facebook</a></p>
                </div>
            </div>
            <div id="button-google" class="social-icons">
                <div class="si-icons">
<!--                     <i class="fab fa-google fa-2x"></i> -->
                </div>
                <div class="si-texts">
                    <p><a href="#">Sign up with Google</a></p>
                </div>
            </div>
        </div>
    </article>
    <footer class="fixed-footer">
        2019 <i class="material-icons">copyright</i> <a href="https://www.navercorp.com/ko" target="_blank">NAVER.corp</a>
    </footer>
<script src="../../../resources/js/login.js"></script>
</body>
</html>