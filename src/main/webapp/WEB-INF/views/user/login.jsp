<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>LOGIN</title>
<jsp:include page="/WEB-INF/views/common/head.jsp"></jsp:include>
</head>
<body class="main-div">
	<div id="login-bg">
		<div id="login-div">
			<div id="login-title-div">
				<h1 id="login-title" class="animated rubberBand">Welcome, Back!</h1>
			</div>
			<div id="login-main">
				<div id="uiId-label" class="login-labels"><label for="uiIO">ID</label></div>
				<input type="text" id="login-uiId" value="jen-lim" maxlength="10">
				<div id="uiPwd-label" class="login-labels"><label for="uiPwd">Password</label></div>
				<input type="password" id="login-uiPwd" value="12345678" maxlength="10">
				<button id="login-button">Login</button>
			</div>
			<div id="login-divider">
				<div id="ld-top"></div>
				<div id="ld-bottom"></div>
			</div>
			<div id="signup-div">
				<p>Don't have an account? Sign up <span id="move-to-signup">here</span>.</p>
			</div>
		</div>
		<div class="photo-credit">
			<span>Photo by <a href="https://unsplash.com/@grakozy" target="_blank">Greg Rakozy</a> on <a href="https://unsplash.com/" target="_blank">Unsplash</a></span>
		</div>
	</div>
<script src="../../../resources/js/login.js"></script>
</body>
</html>