<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>SIGN UP</title>
<jsp:include page="/WEB-INF/views/common/head.jsp"></jsp:include>
</head>
<body class="main-div">
	<div id="signup-bg">
		<div id="signup-div">
			<!-- event는 무조건 소문자로 통일! 그래서 onSubmit이 아님! -->
			<div id="signup-title-div">
				<h1 id="signup-title" class="animated rollIn">Create Your Account</h1>
			</div>
			<div id="signup-main">
				<div class="signup-labels" id="signup-uiName-label">Name</div>
				<input type="text" name="ui_name" id="signup-uiName" />
				<div class="signup-labels" id="signup-uiId-label">ID</div>
				<input type="text" name="ui_id" id="signup-uiId" />
				<div class="signup-labels" id="signup-uiPwd-label">Password</div>
				<input type="password" name="ui_pwd" id="signup-uiPwd" />
				<div class="signup-labels" id="signup-uiPwdCheck-label">Password Check</div>
				<input type="password" name="ui_pwd_check" id="signup-uiPwdCheck" />
				<button id="signup-button" class="signup-buttons" onclick="checkForm()">Sign Up</button>
				<button id="signup-index-button" class="signup-buttons" onclick="goIndexPage('/')">Index Page</button>
			</div>
		</div>
		<div id="signup-info">
			<p>Your password is stored in a hashed form.</p>
		</div>
		<div class="photo-credit">
			<span>Photo by <a href="https://unsplash.com/@bbergher" target="_blank">Bruno Bergher</a> on <a href="https://unsplash.com/" target="_blank">Unsplash</a></span>
		</div>
	</div>
</body>
<body>
<script src="../../../resources/js/signup.js"></script>
</body>
</html>