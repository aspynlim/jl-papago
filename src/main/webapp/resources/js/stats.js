function dataLoad(data){
//	console.log("***** data *****");
//	console.log(data);	
	
	ajax({
		url: "/papago/stats?" + data,
		method: 'GET',
		data: data,
		success: function(res){
			var html = '';
			for(var item of res){
				html += '<tr>';
				html += '<td>' + item.psNum + '</td>';
				html += '<td>' + item.uiNum + '</td>';
				html += '<td>' + item.uiId + '</td>';
				html += '<td>' + item.piNum + '</td>';
				html += '<td>' + item.credat + '</td>';
				html += '</tr>';
			}
			$('#tBody').html(html);
		},
		error: function(res){
			console.log(res);
		}
	})
}

function dataLoadByDate(data){
	ajax({
		url: "/papago/stats/date?" + data,
		method: 'GET',
		data: data,
		success: function(res){
			console.log(res);
			var html = '';
			for(var item of res){
				html += '<tr>';
				html += '<td>' + item.credat + '</td>';
				html += '<td>' + item.cnt + '</td>';
				html += '</tr>';
			}
			$('#tBody-credat').html(html);
		},
		error: function(res){
			console.log(res);
		}
	})
}

function dataLoadByUser(data){
	ajax({
		url: "/papago/stats/user?" + data,
		method: 'GET',
		data: data,
		success: function(res){
			console.log(res);
			var html = '';
			for(var item of res){
				html += '<tr>';
				html += '<td>' + item.uiNum + '</td>';
				html += '<td>' + item.uiId + '</td>';
				html += '<td>' + item.cnt + '</td>';
				html += '</tr>';
			}
			$('#tBody-user').html(html);
		},
		error: function(res){
			console.log(res);
		}
	})
}

$(document).ready(function(){
	$('button[data-target]').on('click', function(){
		$('table[data-id]').css('display', 'none');
		$('table[data-id=' + this.getAttribute('data-target') + ']').css('display', '');
		
		if(this.getAttribute('data-target') == 'credat'){
			dataLoadByDate();
		} else if(this.getAttribute('data-target') == 'user'){
			dataLoadByUser();
		}
	})
	
	console.log($('th[data-order]'));
	
	$('th[data-order]').on('click', function(){ 
		var text = this.innerHTML;
		// console.log(text);
		var symbol = text.substring(text.length-1, text.length);
		// console.log(symbol);
		var order = this.getAttribute('data-order');
		// console.log(order);

		if(symbol == '▼'){
		 	this.innerText = text.substring(0, text.length-1) + '▲';
		 	order = order.substring(0, order.indexOf('desc')) + 'asc';
			this.setAttribute('data-order', order);
		} else if(symbol == '▲') {
		 	this.innerText = text.substring(0, text.length-1) + '▼';
		 	order = order.substring(0, order.indexOf('asc')) + 'desc';
			this.setAttribute('data-order', order);
		}
		var param = 'order='+ this.getAttribute('data-order');

		// console.log(this);	// th
		// console.log(this.parentNode);	// tr
		// console.log(this.parentNode.parentNode);	// thead
		// console.log(this.parentNode.parentNode.parentNode);		// table

		if(this.parentNode.parentNode.parentNode.getAttribute('data-id') == 'credat'){
			dataLoadByDate(param);
		} else if(this.parentNode.parentNode.parentNode.getAttribute('data-id') == 'user'){
			dataLoadByUser(param);
		} else {
			dataLoad(param);
		}
	})
	dataLoad();
})