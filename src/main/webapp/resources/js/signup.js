function insertUser(){
	var xhr = new XMLHttpRequest();
	xhr.open('POST', '/signup');
	xhr.setRequestHeader('Content-Type', 'application/json');
	xhr.onreadystatechange = function(){
		if(xhr.readyState == 4 && xhr.status == 200){
			 console.log(xhr.responseText); // {"cnt":"1"}
			var res = JSON.parse(xhr.responseText);
			 console.log(res.cnt); // 1
			 console.log(res); // {cnt: "1"}
	 	 	if(res.cnt == 1){
	 	 		Swal.fire({
	 	 			position: 'top',
	 	 			icon: 'success',
	 	 			title: 'Congratulations!',
	 	 			text: 'You successfully signed up.'
	 	 		}).then(() => {
		 	 		location.href = '/views/user/login';
	 	 		})
	 	 	}
		}

	}
	var param = {
		uiName : document.getElementById('signup-uiName').value,
		uiId : document.getElementById('signup-uiId').value,
		uiPwd : document.getElementById('signup-uiPwd').value
	}
	param = JSON.stringify(param);
	console.log("param stringify : " + param);
	console.log(typeof(param));
	xhr.send(param);
}	

function checkForm(){
	// getElementById 대신 querySelector를 더 많이 씀
	var uiName = document.getElementById("signup-uiName");
	var uiId = document.getElementById("signup-uiId");
	var uiPwd = document.getElementById("signup-uiPwd");
	var uiPwdCheck = document.getElementById("signup-uiPwdCheck");
	
	if(uiName.value.trim().length < 2){
		Swal.fire({
			position: 'top',
			icon: 'error',
			title: 'ERROR',
			text: 'A name should contain more than 2 characters !'
		})
		uiName.value = "";
		uiName.focus();
		return false;
	}
	if(uiId.value.trim().length < 5){
		Swal.fire({
			position: 'top',
			icon: 'error',
			title: 'ERROR',
			text: 'An ID should contain more than 5 characters !'
		})
		uiId.value = "";
		uiId.focus();
		return false;
	}
	if(uiPwd.value.trim().length < 5){
		Swal.fire({
			position: 'top',
			icon: 'error',
			title: 'ERROR',
			text: 'A password should contain more than 5 characters !'
		})
		uiPwd.value = "";
		uiPwd.focus();
		return false;
	}
	if(uiPwd.value != uiPwdCheck.value){
		Swal.fire({
			position: 'top',
			icon: 'error',
			title: 'ERROR',
			text: 'Passwords do not match !'
		})
		uiPwdCheck.value = "";
		uiPwdCheck.focus();
		return false;
	}	
	insertUser();
}