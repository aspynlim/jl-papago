/**
 * HTTP(protocol) : 규약('약속'보다 강제력은 있지만 법은 아님) : 지키려면 header(내가 어떤 형태의 무언가이다 / 어딘가에서 쓸려고 만들었구나.)와 body(key-value)가 필요하다 
 */
function ajax(conf){
	conf.beforeSend = function(xhr){
		xhr.setRequestHeader('x-ajax', 'true');
	}
	$.ajax(conf);
}