$(document).ready(function(){
	// alert(this.innerText); // 버튼이 여러개일때 다음 걸 찾아감
	
	$('button').on('click', function(){
		var param = {
			uiId: $('#login-uiId').val(),
			uiPwd: $('#login-uiPwd').val()
		}
		param = JSON.stringify(param);
		
		$.ajax({
			url: '/login',
			method: 'POST',
			data: param,
			beforeSend: function(xhr){
				xhr.setRequestHeader('x-ajax', true);
			},
			contentType: 'application/json',
			success: function(res){
				if(res.msg.includes("Welcome")){
		 	 		Swal.fire({
		 	 			position: 'top',
		 	 			icon: 'success',
		 	 			title: res.msg,
		 	 			text: 'You successfully logged in !'
		 	 		}).then(() => {
			 	 		location.href = '/views/papago/index';
		 	 		})	
				} else {
		 	 		Swal.fire({
		 	 			position: 'top',
		 	 			icon: 'error',
		 	 			title: res.msg,
		 	 			text: 'Please enter correct information.'
		 	 		}).then(() => {
			 	 		location.href = '/views/user/login';
		 	 		})					
				}
			},
			error: function(res){
	 	 		console.log(res);
			}
		});
	});
});

var signupText = document.getElementById('move-to-signup'); 

signupText.addEventListener('click', function(){ 
	location.href="/views/user/signup";
}, false);