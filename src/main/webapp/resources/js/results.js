function deleteUsers(){
	var checks = document.querySelectorAll('input[name=remove]:checked');
	console.log(checks);
	
	if(checks.length<1){
		Swal.fire({
			position: 'top',
			icon: 'error',
			title: 'ERROR',
			text: 'Please select at least one item to delete !'
		})
		return;
	}
	var piNums = '';
	for(var i=0; i<checks.length; i++){
		piNums += 'piNums=' + [checks[i].value] + '&';
	}
	console.log(piNums);
	
	var xhr2 = new XMLHttpRequest();
	xhr2.open('DELETE', '/papago?' + piNums);
	xhr2.setRequestHeader('Content-Type','application/json');

	xhr2.onreadystatechange = function(){
		if(xhr2.readyState == 4 && xhr2.status == 200){
			var res = JSON.parse(xhr2.responseText);
	 	 	if(res.cnt != 0){
				Swal.fire({
					position: 'top',
					icon: 'success',
					title: 'Deleted!',
					text: '# of Deleted Item(s) : ' + res.cnt
				}).then(() => {
					location.href = '/views/papago/index?page=results';
				})
	 	 	}
		}
	}
	 xhr2.send(piNums);
}

function selectAll(){
	var selectAll = document.getElementById('checkAll').checked;
	var checkOptions = document.getElementsByClassName('checkOne');
	var intLength = checkOptions.length;
	for(var i=0; i<intLength; i++){
		var checkOption = checkOptions[i];
		checkOption.checked = selectAll;
	}
}

function dataLoad(data){
//	console.log("***** data *****");
//	console.log(data);
	
	var xhr = new XMLHttpRequest();
	xhr.open('GET', '/papago?' + data);
	xhr.setRequestHeader('Content-Type', 'application/json');
	xhr.onreadystatechange = function(){
		if(xhr.readyState == 4 && xhr.status == 200){
//			console.log("xhr.responseText" + xhr.responseText);

			var list3 = JSON.parse(xhr.responseText);
			var tBody = document.getElementById('tBody');
			var html = '';
			for(var item of list3){
				html += '<tr>';
				html += '<td><input type="checkbox" name="remove" value="' + item.piNum + '"></td>';
				html += '<td>' + item.piNum + '</td>';
				html += '<td>' + item.piSource + '</td>';
				html += '<td>' + item.piTarget + '</td>';
				html += '<td>' + item.piText + '</td>';
				html += '<td>' + item.piResult + '</td>';
				html += '<td>' + item.piCnt + '</td>';
				html += '</tr>';
			}
			tBody.innerHTML = html;
		}
	}
	xhr.send();
}

window.onload = function(){
	var btn = document.querySelector("#search-button");
	btn.onclick = function(){
		var checks = document.querySelectorAll("[name=search]:checked");
		console.log(checks);
		
		console.log("search types # : " + checks.length);
		if(checks.length == 0){
			Swal.fire({
				position: 'top',
				icon: 'error',
				title: 'ERROR',
				text: 'Please select at least one checkbox !'
			})
			return;
		}
		var searchStr = document.querySelector("#searchStr");
		if(searchStr.value.trim().length < 1){
			Swal.fire({
				position: 'top',
				icon: 'error',
				title: 'ERROR',
				text: 'Please enter a search keyword !'
			})
			return;
		}
		var search = '';
		for(var i=0; i<checks.length; i++){
			search += 'search=' + checks[i].value + '&';
		}
		search += 'searchStr=' + searchStr.value;
//		getUserList(search);
		dataLoad(search);
	}
	
	var dataOrder = document.querySelectorAll('th[data-order]');
	console.log(dataOrder);
	
	for(var i=0; i<dataOrder.length; i++){
		dataOrder[i].onclick = function(){ 	
			var text = this.innerHTML;
			// console.log(text);
			var symbol = text.substring(text.length-1, text.length);
			// console.log(symbol);
			var order = this.getAttribute('data-order');
			// console.log(order);
			
			if(symbol == '▼'){
			 	this.innerText = text.substring(0, text.length-1) + '▲';
			 	order = order.substring(0, order.indexOf('desc')) + 'asc';
				this.setAttribute('data-order', order);
			} else if(symbol == '▲') {
			 	this.innerText = text.substring(0, text.length-1) + '▼';
			 	order = order.substring(0, order.indexOf('asc')) + 'desc';
				this.setAttribute('data-order', order);
			}
			var param = 'order='+ this.getAttribute('data-order');
			dataLoad(param);
		}
	}
	dataLoad();
}