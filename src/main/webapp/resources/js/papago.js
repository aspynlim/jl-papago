function goPage(url){
	location.href = '/views' + url;
}

$(document).ready(function(){
	$('#papago-button').on('click', function(){
		// same as VO
		var param = {
			source : $('#source option:selected').val(),
			target : $('#target option:selected').val(),
			text: document.querySelector('#text').value
		}
		console.log(param);
		var conf = {
			contentType: 'application/json; charset-utf-8',
			url: '/papago',
			method: 'POST',
			dataType: 'json',
			data: JSON.stringify(param),
			success: function(res){
				if(res.msg) console.log(res.msg);
				$('#result').val(res.result.translatedText);
			},
			error: function(res){
				console.log(res);
			}
		}
		ajax(conf);
	});
});